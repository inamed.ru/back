<!DOCTYPE html>
<html lang="ru">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Как диагностировать простатит у мужчин? Отвечают врачи урологической клиники &#34;Инамед&#34;: современные методы лечения простатита, синдрома хронической тазовой боли, высококвалифицированные специалисты, комплексная диагностика. Запись на прием по телефону">
  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
  <link rel="stylesheet" href="/local/front/template/styles/style.core.css?v=<?php require __DIR__ . '/version.php'?>"/>
  <link rel="stylesheet" href="/local/front/template/styles/diagnostic.core.css?v=<?php require __DIR__ . '/version.php'?>"/>
    <title>Диагностика простатита у мужчин - как диагностировать заболевания предстательной железы</title>

    <!-- Метрика -->
    <?php require __DIR__ . '/includes/metrika.php' ?>
</head>
  <body>
    <header class="header">
      <div class="container header__inner">
        <div class="content">
          <a class="logo header__logo" href="/"></a>
            <div class="header__name"> <a href="/">Инамед </a><span>Клиника урологии</span></div><a class="header__call" href="tel:+74952041494" aria-label="Позвонить"></a>
            <button class="header__switcher js-header-switcher hidden-l" type="button" aria-label="Открыть или закрыть мобильное меню"></button>
            <?php require __DIR__ . '/includes/menu.php'?>

        </div>
      </div>
      <div class="container header__bottom">
        <div class="content">
          <p class="text-marked text-marked--size--l text-align-center">Излечиваем хронический простатит (СХТБ) с <strong>эффективностью 87%</strong>.
          </p>
        </div>
      </div>
    </header>
    <main>
      <div class="container breadcrumbs">
        <div class="content breadcrumbs__inner"><a class="breadcrumbs__parent" href="/">Главная</a>
          <div class="breadcrumbs__current">Диагностика</div>
        </div>
      </div>
      <section class="container page-section">
        <div class="content">
          <h1>Диагностика простатита у мужчин</h1>
          <div class="accordion-item page-subsection">
            <div class="accordion-item__header">Оглавление
              <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                  <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                  <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                </path>
              </svg>
            </div>
            <div class="accordion-item__body">
              <ul class="unordered-list">
                <li><a href="#diagnostic-1">Классификация простатита</a></li>
                <li><a href="#diagnostic-2">Какие заболевания должны быть исключены при диагностике?</a></li>
                <li><a href="#diagnostic-3">Когда нужно пройти диагностику?</a></li>
                <li><a href="#diagnostic-4">Этапы диагностики</a></li>
                <li><a href="#diagnostic-5">Домашняя диагностика простатита</a></li>
                <li><a href="#diagnostic-6">Как попасть на лечение?</a></li>
                <li><a href="#diagnostic-7">Цены</a></li>
                <li><a href="#diagnostic-8">Частые вопросы</a></li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--background-color">
        <div class="content">
          <div class="grid grid--align-items--center grid--justify--space-between">
            <ul class="unordered-list unordered-list--marked grid__cell grid__cell--m--7 grid__cell--s--5 grid__cell--xs--12">
              <li>Обследование — в день визита</li>
              <li>Минимальный набор исследований</li>
              <li>Деликатный подход без стресса</li>
            </ul><a class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12 button" href="tel:+74952041494">Записаться на консультацию</a>
          </div>
        </div>
      </section>
      <section class="container page-section">
        <div class="content">
          <div class="page-subsection">
            <picture>
              <source srcset="/local/front/images/diagnostic-1_s.webp, /local/front/images/diagnostic-1_s@2x.webp 2x" media="(max-width: 479px)"/>
              <source srcset="/local/front/images/diagnostic-1_l.webp, /local/front/images/diagnostic-1_l@2x.webp 2x" media="(min-width: 480px) and (max-width: 999px)"/>
              <source srcset="/local/front/images/diagnostic-1_xl.webp, /local/front/images/diagnostic-1_xl@2x.webp 2x" media="(min-width: 1000px)"/>
              <source srcset="/local/front/images/diagnostic-1_s.jpg, /local/front/images/diagnostic-1_s@2x.jpg 2x" media="(max-width: 479px)"/>
              <source srcset="/local/front/images/diagnostic-1_l.jpg, /local/front/images/diagnostic-1_l@2x.jpg 2x" media="(min-width: 480px) and (max-width: 999px)"/>
              <source srcset="/local/front/images/diagnostic-1_xl.jpg, /local/front/images/diagnostic-1_xl@2x.jpg 2x" media="(min-width: 1000px)"/><img src="/local/front/images/diagnostic-1.jpg" loading="lazy" decoding="async"/>
            </picture>
          </div>
          <div class="page-subsection">
            <p>Простатит — это болезнь в острой или хронической форме, связанная с воспалительными процессами в простате, которая проявляется характерной клинической картиной.</p>
            <p><strong>Диагностика простатита</strong> — важный этап для определения формы заболевания и назначения верного лечения.</p>
            <p>Наиболее уязвимым органом в репродуктивной системе мужчины является простата.</p>
            <p>30% от всех обращений к урологу приходится на патологические процессы в предстательной железе, частота которых в несколько раз увеличивается после 40-45 лет.</p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="diagnostic-1">
        <div class="content">
          <h2>Классификация простатита</h2>
          <div class="page-subsection">
            <p>В 1995 году NIH (Национальным институтом здоровья США) разработал следующую классификацию.</p>
            <ul class="unordered-list par">
              <li>категория I — острый простатит (ОБП);</li>
              <li>категория II — хронический бактериальный простатит;</li>
              <li>категория III — хронический небактериальный простатит/синдром хронической тазовой боли
                <ul class="unordered-list par">
                  <li>категория IIIА — воспалительный СХТБ (лейкоциты в эякуляте/ секрете простаты/третьей порции мочи);</li>
                  <li>категория IIIБ — невоспалительный синдром хронической тазовой боли (нет лейкоцитов в эякуляте/секрете простаты/третьей порции мочи);</li>
                </ul>
              </li>
              <li>категория IV — асимптоматический воспалительный простатит (гистологический простатит).</li>
            </ul>
          </div>
          <div class="page-subsection">
            <h3>Хронический небактериальный простатит / синдром хронической тазовой боли (СХТБ)</h3>
            <p>Это <strong>самый противоречивый</strong> и наименее аргументированный раздел классификации.</p>
          </div>
          <div class="block-marked page-subsection">
            <p>Данная категория простатита является <strong>наиболее распространенной</strong> среди всех форм (90% всех случаев).</p>
          </div>
          <div class="page-subsection">
            <p>В действительности простатит — <strong>довольно запутанная</strong> научная и практическая проблема. <strong>Специалистов</strong>, которые разбираются в ней глубоко и всесторонне <strong>очень мало</strong>.</p>
          </div>
          <div class="block-marked page-subsection">
            <p>Клиника "Инамед" – центр компетенций в лечении хронического простатита (синдрома хронической тазовой боли).</p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="diagnostic-2">
        <div class="content">
          <h2>Какие заболевания должны быть исключены при диагностике?</h2>
          <div class="page-subsection">
            <p>Диагностика острого простатита в большинстве случаев не представляет сложностей.</p>
            <p>Хроническое же воспаление нередко требует проведения расширенной <strong>дифференциальной диагностики.</strong></p>
          </div>
          <div class="block-marked page-subsection">
            <p>Мы <strong>не назначаем лишних анализов</strong> и проводим минимальный набор исследований, достаточный для назначения эффективного лечения.</p>
          </div>
          <div class="page-subsection">
            <p>Основными патологиями, от которых следует отличать данное состояние, являются:</p>
            <ul class="unordered-list par">
              <li><strong>воспалительные процессы</strong>, вовлекающие рядом расположенные органы — синдром болезненного мочевого пузыря, везикулит, цистит, уретрит;</li>
              <li><strong>энтерологические заболевания</strong> — геморрой, воспалительные заболевания кишечника (неспецифический язвенный колит, болезнь Крона, прокталгия, проктит) и другие;</li>
              <li><strong>сексуальные расстройства</strong>, не связанные с воспалением простаты;</li>
              <li><strong>неврологический заболевания</strong> — нейропатия полового нерва (пудентальная невралгия);</li>
              <li><strong>скелетно-мышечные заболевания</strong> — миофасциальный синдром, синдром грушевидной мышцы, запирательной мышцы, пубалгия, симфизит (воспаление лобкового соединения)</li>
            </ul>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="diagnostic-3">
        <div class="content">
          <h2>Когда нужно пройти диагностику?</h2>
          <div class="page-subsection">
            <p>Проходить специфические обследования простаты рекомендуется <strong>всем мужчинам старше 40 лет.</strong></p>
          </div>
          <div class="block-marked page-subsection">
            <p>После 40-45 лет проходить пальцевое ректальное исследование простаты необходимо <strong>ежегодно.</strong></p>
          </div>
          <div class="page-subsection">
            <p>С 50 лет мужчинам показан скрининг предстательной железы, который проводят ПСА методом. Дополнительную диагностику заболеваний простаты: анализ секрета, компьютерную томографию, трансректальное ультразвуковое исследование — назначают по показаниям.</p>
            <p>Особенно важно пройти диагностику при характерной симптоматике.</p>
          </div>
          <div class="page-subsection">
            <h3>Длительные симптомы</h3>
            <p>В <strong>первую очередь</strong>, если вас длительное время беспокоит один или несколько следующих симптомов:</p>
          </div>
          <ul class="unordered-list unordered-list--marked grid grid--small-gap--y page-subsection">
            <li class="grid__cell grid__cell--xs--12">Боль в паховой области, которая может отдавать в яичко, половой член, внутреннюю поверхность бедра, копчик, задний проход
            </li>
            <li class="grid__cell grid__cell--xs--12">Боли усиливаются после эякуляции или мочеиспускания
            </li>
            <li class="grid__cell grid__cell--xs--12">Затрудненное или учащенное мочеиспускание, неудержимые позывы
            </li>
            <li class="grid__cell grid__cell--xs--12">Снижение эрекции
            </li>
            <li class="grid__cell grid__cell--xs--12">Наличие желтой слизи в эякуляте
            </li>
          </ul>
          <div class="page-subsection">
            <picture>
              <source srcset="/local/front/images/diagnostic-2.webp"/>
              <source srcset="/local/front/images/diagnostic-2.jpg"/><img src="/local/front/images/diagnostic-2.jpg" loading="lazy" decoding="async"/>
            </picture>
          </div>
          <div class="page-subsection">
            <h3>Нечетко выраженные симптомы</h3>
            <p>Во <strong>вторую очередь</strong>, если у вас есть нечетко выраженные симптомы.</p>
            <p>Человек может не чувствовать сильной боли в паховой области, а воспринимать симптомы, как дискомфорт, раздражение, распирание, жжение и т.д. <strong>Не обращать внимание</strong> на проблемы с мочеиспусканием или эрекцией. Эти признаки могут говорить о наличии проблемы.</p>
            <p>Диагностика поможет выявить заболевание на самых <strong>ранних этапах.</strong></p>
          </div>
          <div class="icon-block icon-block--warning page-subsection">
            <div class="icon-block__inner">
              <p>В случае возникновения первых симптомов дискомфорта <strong>не</strong> нужно <strong>заниматься самолечением</strong>.</p>
              <p>Это может вызвать <strong>серьезные последствия</strong> и усугубить течение болезни.</p>
              <p>Обращайтесь только к проверенным и грамотным специалистам.</p>
            </div>
          </div>
          <div class="page-subsection">
            <h3>После перенесенных заболеваний</h3>
            <p>В <strong>третью очередь</strong>, если вам ранее уже ставили диагноз острый, хронический простатит или синдром хронической тазовой боли.</p>
            <p>Тогда диагностика хронического простатита снизит риса рецидива или обострения.</p>
          </div>
          <div class="page-subsection">
            <h3>Для профилактики</h3>
            <p>Большинство мужчин убеждены, что посещать врача нужно только при появлении неприятных симптомов.</p>
            <p>Однако простатит и другие болезни мочеполовой системы могут длительное время протекать бессимптомно.</p>
            <p>Для диагностики заболеваний предстательной железы среди молодежи практикуют базовый урологический осмотр, поскольку серьезные болезни простаты до 25 лет регистрируют не часто.</p>
            <p>Исключение следует сделать при наличии признаков воспаления органа, а также людям из групп риска:</p>
            <ul class="unordered-list par">
              <li>при наличии инфекционных и воспалительных заболеваний репродуктивной системы в прошлом.</li>
              <li>при малоподвижном образе жизни, сидячей работе, наличии лишнего веса, вредных привычек;</li>
              <li>при подозрении на бесплодие, нерегулярной половой жизни;</li>
              <li>при неблагоприятной наследственности (если заболевания железы диагностировались у близких родственников);</li>
            </ul>
          </div>
          <div class="icon-block icon-block--suitcase page-subsection">
            <div class="icon-block__inner">
              <div class="grid grid--justify--space-between grid--align-items--center">
                <div class="grid__cell grid__cell--l--6 grid__cell--m--8 grid__cell--xs--12">
                  <p><strong>Профилактические визиты</strong> к урологу крайне важны для здоровья!</p>
                  <p>Мужчины должны минимум <strong>раз в 1 год</strong> обследоваться у уролога и в молодом, и в пожилом возрасте.</p>
                </div><a class="grid__cell grid__cell--l--4 grid__cell--m--4 grid__cell--s--8 grid__cell--xs--12 button par" href="tel:+74952041494">Записаться на консультацию</a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="diagnostic-4">
        <div class="content">
          <h2>Этапы диагностики</h2>
          <div class="page-subsection">
            <p>Программа обследования подбирается индивидуально для каждого пациента. Она зависит от возраста, жалоб и симптомов.</p>
            <p>При простатите обследование включает:</p>
            <ul class="unordered-list par">
              <li>Консультацию уролога</li>
              <li>Лабораторные анализы</li>
              <li>Инструментальные обследования</li>
            </ul>
          </div>
          <div class="page-subsection">
            <h3 class="page-subsection">Консультация уролога</h3>
            <ol class="ordered-list ordered-list--marked grid page-subsection">
              <li class="grid__cell grid__cell--xs--12">Специалист внимательно и спокойно выслушает вас. Проведет опрос, уточнит у вас, какие симптомы беспокоят и когда они возникли, особенности половой жизни, мочеиспускания. Изучит данные предыдущих исследований.
              </li>
              <li class="grid__cell grid__cell--xs--12">
                <div>
                  <p>Далее выполнит осмотр половых органов.</p>
                  <p class="page-subsection">После этого врач проведет тщательное ректальное пальцевое исследование простаты и семявыводящих путей.</p>
                  <div class="block-marked block-marked--padding--small block-marked--shifted-for-list--left page-subsection">
                    <p>Мы проводим данное исследование по <strong>определенной методике</strong>, которая позволяет проникнуть более глубоко чтобы достигнуть зоны проекции семявыводящих путей.</p>
                    <p>Большинство урологов <strong>никогда не осматривает эту зону</strong>. А она чаще всего содержит <strong>плотные образования</strong>, которые является источником хронической боли.</p>
                  </div>
                </div>
              </li>
              <li class="grid__cell grid__cell--xs--12">
                <div>
                  <p class="page-subsection">В ходе ректального обследования получают отделяемое семявыводящих путей, который направляют на анализ в лабораторию.</p>
                  <div class="icon-block icon-block--facepalm icon-block--shifted-for-list--left page-subsection">
                    <div class="icon-block__inner">
                      <p>Для наших врачей <strong>нет ничего постыдного</strong> или неудобного в обследовании и лечении урологических заболеваний.</p>
                      <p>Отбросьте стеснение и ни о чём не беспокойтесь. Мы максимально <strong>бережно и корректно</strong> отнесемся к абсолютно любой проблеме.</p>
                    </div>
                  </div>
                </div>
              </li>
              <li class="grid__cell grid__cell--xs--12">
                <div>В конце приёма врач устанавливает предварительный диагноз. Чтобы уточнить его, могут понадобиться дополнительные исследования.</div>
              </li>
            </ol>
          </div>
          <div class="block-marked grid grid--justify--space-between grid--align-items--center page-subsection">
            <div class="grid__cell grid__cell--l--7 grid__cell--m--8 grid__cell--xs--12">
              <h3>Прием (осмотр, консультация) врача уролога</h3>
              <p class="text-marked--size--xl">2500 руб.</p>
            </div><a class="grid__cell grid__cell--l--4 grid__cell--m--4 grid__cell--s--8 grid__cell--xs--12 button par" href="tel:+74952041494">Записаться</a>
          </div>
          <div class="page-subsection">
            <h3>Инструментальная диагностика</h3>
          </div>
          <div class="block-marked page-subsection">
            <p>Благодаря нашим партнёрам, клиника Инамед имеет доступ к высококлассным аппаратам КТ и МРТ.</p>
          </div>
          <div class="page-subsection">
            <picture>
              <source srcset="/local/front/images/diagnostic-3_s.webp, /local/front/images/diagnostic-3_s@2x.webp 2x" media="(max-width: 479px)"/>
              <source srcset="/local/front/images/diagnostic-3_l.webp, /local/front/images/diagnostic-3_l@2x.webp 2x" media="(min-width: 480px) and (max-width: 999px)"/>
              <source srcset="/local/front/images/diagnostic-3_xl.webp, /local/front/images/diagnostic-3_xl@2x.webp 2x" media="(min-width: 1000px)"/>
              <source srcset="/local/front/images/diagnostic-3_s.jpg, /local/front/images/diagnostic-3_s@2x.jpg 2x" media="(max-width: 479px)"/>
              <source srcset="/local/front/images/diagnostic-3_l.jpg, /local/front/images/diagnostic-3_l@2x.jpg 2x" media="(min-width: 480px) and (max-width: 999px)"/>
              <source srcset="/local/front/images/diagnostic-3_xl.jpg, /local/front/images/diagnostic-3_xl@2x.jpg 2x" media="(min-width: 1000px)"/><img src="/local/front/images/diagnostic-3.jpg" loading="lazy" decoding="async"/>
            </picture>
          </div>
          <div class="page-subsection">
            <p>Во время полного обследования мужского организма пациента используем следующие аппаратные диагностические процедуры:</p>
            <ul class="unordered-list par">
              <li>УЗИ-диагностика<br>Используем запатентованную методику — бимануально-компрессионный метод ультразвукового сканирования, позволяющий визуализировать инфильтративные образования.</li>
              <li>МРТ-диагностика<br>Имеет преимущества в исследовании мягких тканей.</li>
              <li>Урофлоуметрия<br>Помогает получить информацию об отклонении процесса мочеиспускания от нормы</li>
            </ul>
          </div>
          <div class="page-subsection">
            <h3>Лабораторная диагностика</h3>
            <p>В одно месте вы можете на базе урологической лаборатории нашего партнера — сети диагностических центров Горлаб сдать любые анализы.</p>
          </div>
          <div class="block-marked block-marked--padding--small page-subsection">
            <p><strong>Уникальная методика</strong> взятия и интерпретации анализов отделяемого семявыводящих путей, позволяет гарантировать <strong>достоверность результатов.</strong></p>
          </div>
          <div class="page-subsection">
            <p>Ошибки в результатах анализов влекут за собой неправильную тактику лечения, что отдаляет пациента от желаемого результата и тратит в пустую его время и деньги.</p>
          </div>
          <div class="page-subsection">
            <picture>
              <source srcset="/local/front/images/diagnostic-4_s.webp, /local/front/images/diagnostic-4_s@2x.webp 2x" media="(max-width: 479px)"/>
              <source srcset="/local/front/images/diagnostic-4_l.webp, /local/front/images/diagnostic-4_l@2x.webp 2x" media="(min-width: 480px) and (max-width: 999px)"/>
              <source srcset="/local/front/images/diagnostic-4_xl.webp, /local/front/images/diagnostic-4_xl@2x.webp 2x" media="(min-width: 1000px)"/>
              <source srcset="/local/front/images/diagnostic-4_s.jpg, /local/front/images/diagnostic-4_s@2x.jpg 2x" media="(max-width: 479px)"/>
              <source srcset="/local/front/images/diagnostic-4_l.jpg, /local/front/images/diagnostic-4_l@2x.jpg 2x" media="(min-width: 480px) and (max-width: 999px)"/>
              <source srcset="/local/front/images/diagnostic-4_xl.jpg, /local/front/images/diagnostic-4_xl@2x.jpg 2x" media="(min-width: 1000px)"/><img src="/local/front/images/diagnostic-4.jpg" loading="lazy" decoding="async"/>
            </picture>
          </div>
          <div class="block-marked page-subsection">
            <p>Мы <strong>не просим пересдавать</strong> у нас все анализы, которые вы уже выполняли.</p>
          </div>
          <div class="page-subsection">
            <p>Минимальный список анализов для оценки причин хронического простатита (синдрома хронической тазовой боли):</p>
            <ul class="unordered-list par">
              <li>Микроскопическое исследование отделяемого семявыводящих путей и секрет простаты</li>
              <li>Бактериальный посев отделяемого семявыводящих путей и секрет простаты</li>
            </ul>
            <p>Также могут потребоваться следующие анализы:</p>
            <ul class="unordered-list par">
              <li>Общий анализ мочи с микроскопией осадка</li>
              <li>Анализа на заболевания передающиеся половым путем</li>
            </ul>
          </div>
          <div class="block-marked block-marked--padding--small page-subsection">
            <p>Результаты обследований пациент получит в <strong>сжатые сроки</strong> и с доступной для понимания <strong>расшифровкой.</strong></p>
          </div>
          <div class="page-subsection">
            <p>По результатам комплексной диагностики выдадим <strong>экспертное заключение врача.</strong></p>
            <p>Если заболевание подтверждено, <strong>объясним схему лечения</strong>, назначим процедуры и препараты.</p>
            <p>Каждый вид диагностики даёт часть необходимой информации. Чем больше сведений соберёт доктор, тем эффективнее он составит курс лечения.</p>
          </div>
        </div>
      </section>
      <section class="container page-section" id="diagnostic-5">
        <div class="content">
          <h2>Домашняя диагностика простатита</h2>
          <div class="page-subsection">
            <p>Мы не рекомендуем самодиагностику, поскольку выполнить ее в домашних условиях невозможно. Для постановки диагноза следует обратиться к врачу-урологу.</p>
          </div>
        </div>
      </section>
      <section class="container page-section home-treatment" id="diagnostic-6">
        <div class="content">
          <h2>Как попасть на лечение?</h2>
          <div class="slider page-subsection">
            <div class="slider__slides swiper">
              <div class="slider__wrapper swiper-wrapper">
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Консультация
                    </p>
                    <div class="home-treatment-item__tel">По телефону: <span>+7 (495) 204-14-94</span></div>
                    <div class="home-treatment-item__text">Медицинский консультант расспросит о симптомах, подберет подходящего врача, расскажет про стоимость процедур и запишет на первичный прием.</div>
                    <div class="home-treatment-item__duration">5 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Прием специалиста
                    </p>
                    <div class="home-treatment-item__text">Врач внимательно и спокойно выслушает вас, изучит данные предыдущих исследований, назначит только необходимую диагностику и поставит предварительный диагноз.</div>
                    <div class="home-treatment-item__duration">от 30 до 40 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Диагностика в тот же день
                    </p>
                    <div class="home-treatment-item__text">Врач выполнит осмотр предстательной железы и семявыводящих путей, возьмет биоматериал для анализов. <br> На основе полученных данных врач сможет назначить индивидуальный курс лечения.</div>
                    <div class="home-treatment-item__duration">от 10 до 20 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Повторный прием и лечение
                    </p>
                    <div class="home-treatment-item__text">Врач подробно расскажет о результатах анализа, детально объяснит суть курса лечения, опишет процедуры и препараты. <br> В этот же день вы сможете начать лечение.</div>
                    <div class="home-treatment-item__duration">от 20 до 40 минут</div>
                  </div>
                </div>
              </div>
              <div class="slider__arrow slider__arrow--prev"></div>
              <div class="slider__arrow slider__arrow--next"></div>
              <div class="slider__pagination"></div>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="diagnostic-7">
        <div class="content">
          <h2>Цены</h2>
          <div class="page-subsection">
            <div class="table-wrap">
              <table class="table">
                <colgroup class="table__colgroup table__colgroup--2">
                  <col class="table__col"/>
                  <col class="table__col"/>
                </colgroup>
                <tr class="table__row">
                  <td class="table__cell">Прием (осмотр консультация) врача-уролога первичный
                  </td>
                  <td class="table__cell text-align-center">3500 руб.
                  </td>
                </tr>
                <tr class="table__row">
                  <td class="table__cell">Прием (осмотр консультация) врача-уролога вторичный
                  </td>
                  <td class="table__cell text-align-center">2500 руб.
                  </td>
                </tr>
              </table>
            </div>
            <p class="text-marked text-marked--size--s text-marked--color--grey">Указанные на сайте цены не являются публичной офертой. Стоимость услуг зависит от уровня квалификации специалиста.
            </p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="diagnostic-8">
        <div class="content">
          <h2>Частые вопросы</h2>
          <div class="page-subsection grid">
            <div class="grid__cell grid__cell--m--9 grid__cell--xs--12">
              <div class="accordion-item par">
                <div class="accordion-item__header">Как долго длится лечение?
                  <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                      <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                      <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                    </path>
                  </svg>
                </div>
                <div class="accordion-item__body">
                  <p>Длительность лечения зависит от&nbsp;времени, которое вы&nbsp;болеете, а&nbsp;также от&nbsp;реакции организма на&nbsp;процесс лечения.</p>
                </div>
              </div>
              <div class="accordion-item par">
                <div class="accordion-item__header">Какие методы вы&nbsp;используете для лечения?
                  <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                      <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                      <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                    </path>
                  </svg>
                </div>
                <div class="accordion-item__body">
                  <p>Мы&nbsp;используем в&nbsp;основном физиотерапевтические методы лечения. Но&nbsp;область воздействия, а&nbsp;также способ принципиально отличается от&nbsp;обычных методов лечения хронического простатита.</p>
                </div>
              </div>
              <div class="accordion-item par">
                <div class="accordion-item__header">Почему я&nbsp;раньше не&nbsp;слышал о&nbsp;данной методике?
                  <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                      <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                      <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                    </path>
                  </svg>
                </div>
                <div class="accordion-item__body">
                  <p>Долгое время данная методика применялась только в&nbsp;одной клинике в&nbsp;стране и&nbsp;никак не&nbsp;рекламировалась. Информация передавалась по&nbsp;&laquo;сарафанному радио&raquo; от&nbsp;пациента к&nbsp;пациенту.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <footer class="container page-section page-section--background-color footer">
      <div class="content footer__inner">
        <div class="footer__row footer__row--name">
          <div class="footer__name">Инамед <span>Клиника урологии</span></div>
          <div class="footer__slogan">Лечим иначе!</div>
        </div>
        <!-- <div class="footer__row footer__row--license footer__item footer__item--license">
          a(href='#') Лицензии и сертификаты
        </div> -->
        <div class="footer__row footer__row--copyright">
          <div class="footer__copyright">
            <p class="footer__year">2024, ООО &laquo;Инамед&raquo;</p><a href="#">Правила использования сайтом</a><a href="#">Соглашение на обработку персональных данных</a>
          </div>
          <div class="footer__info">Имеются противопоказания, необходима консультация специалиста.</div>
          <div class="footer__info" style="display: block">Наша клиника располагается на базе сети диагностических центров <a href="https://gorlab.ru/" target="_blank">«Горлаб»</a></div>
        </div>
      </div>
    </footer>
  </body>
  <link rel="stylesheet" href="/local/front/template/styles/style.rest.css?v=<?php require __DIR__ . '/version.php'?>"/>
  <link rel="stylesheet" href="/local/front/template/styles/diagnostic.rest.css?v=<?php require __DIR__ . '/version.php'?>"/>
  <script src="/local/front/template/scripts/script.js?v=<?php require __DIR__ . '/version.php'?>"></script>
  <script src="/local/front/template/scripts/diagnostic.js?v=<?php require __DIR__ . '/version.php'?>"></script>
</html>
