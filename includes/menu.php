<nav class="header-navigation header__navigation">
    <ul class="header-navigation__list">
        <li class="header-navigation__item"><a href="/o-klinike/">О клинике</a>
        </li>
        <li class="header-navigation__item"><a href="/urology/">Мы лечим</a>
        </li>
        <li class="header-navigation__item"><a href="/metody-lecheniya/">Методы</a>
        </li>
        <li class="header-navigation__item"><a href="/diagnostika/">Диагностика</a>
        </li>
        <li class="header-navigation__item"><a href="/pacientam/">Пациентам</a>
        </li>
        <li class="header-navigation__item"><a href="/kontakty/">Контакты</a>
        </li>
    </ul>
</nav>
