<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/local/front/template/styles/style.core.css?v=<?php require __DIR__ . '/version.php'?>"/>
    <link rel="stylesheet" href="/local/front/template/styles/methods.core.css?v=<?php require __DIR__ . '/version.php'?>"/>
    <title>Инамед: Методы лечения</title>
      <!-- Метрика -->
      <?php require __DIR__ . '/includes/metrika.php' ?>
  </head>
  <body>
    <header class="header">
      <div class="container header__inner">
        <div class="content">
          <a class="logo header__logo" href="/"></a>
            <div class="header__name"> <a href="/">Инамед </a><span>Клиника урологии</span></div><a class="header__call" href="tel:+74952041494" aria-label="Позвонить"></a>
            <button class="header__switcher js-header-switcher hidden-l" type="button" aria-label="Открыть или закрыть мобильное меню"></button>
            <?php require __DIR__ . '/includes/menu.php'?>
        </div>
      </div>
      <div class="container header__bottom">
        <div class="content">
          <p class="text-marked text-marked--size--l text-align-center">Излечиваем хронический простатит (СХТБ) с <strong>эффективностью 87%</strong>.
          </p>
        </div>
      </div>
    </header>
    <main>
      <div class="container breadcrumbs">
        <div class="content breadcrumbs__inner"><a class="breadcrumbs__parent" href="/">Главная</a>
          <div class="breadcrumbs__current">Методы лечения</div>
        </div>
      </div>
      <section class="container page-section">
        <div class="content">
          <h1>Методы лечения</h1>
          <div class="accordion-item page-subsection">
            <div class="accordion-item__header">Оглавление
              <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                  <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                  <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                </path>
              </svg>
            </div>
            <div class="accordion-item__body">
              <ul class="unordered-list">
                <li><a href="#methods-1">Стратегия лечения</a></li>
                <li><a href="#methods-2">Преимущества нашей методики</a></li>
                <li><a href="#methods-3">Процедуры</a></li>
                <li><a href="#methods-4">Как попасть на лечение?</a></li>
                <li><a href="#methods-5">Цены</a></li>
                <li><a href="#methods-6">Частые вопросы</a></li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--background-color">
        <div class="content">
          <div class="grid grid--align-items--center grid--justify--space-between">
            <ul class="unordered-list unordered-list--marked grid__cell grid__cell--m--7 grid__cell--s--5 grid__cell--xs--12">
              <li>Революционная методика лечения</li>
              <li>Специализация в данной сфере</li>
              <li>Внимание к вашей проблеме</li>
            </ul><a class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12 button" href="tel:+74952041494">Записаться на консультацию</a>
          </div>
        </div>
      </section>
      <section class="container page-section">
        <div class="content">
          <div class="page-subsection">
            <picture>
              <source srcset="/local/front/images/methods-1_s.webp, /local/front/images/methods-1_s@2x.webp 2x" media="(max-width: 479px)"/>
              <source srcset="/local/front/images/methods-1_l.webp, /local/front/images/methods-1_l@2x.webp 2x" media="(min-width: 480px) and (max-width: 999px)"/>
              <source srcset="/local/front/images/methods-1_xl.webp, /local/front/images/methods-1_xl@2x.webp 2x" media="(min-width: 1000px)"/>
              <source srcset="/local/front/images/methods-1_s.jpg, /local/front/images/methods-1_s@2x.jpg 2x" media="(max-width: 479px)"/>
              <source srcset="/local/front/images/methods-1_l.jpg, /local/front/images/methods-1_l@2x.jpg 2x" media="(min-width: 480px) and (max-width: 999px)"/>
              <source srcset="/local/front/images/methods-1_xl.jpg, /local/front/images/methods-1_xl@2x.jpg 2x" media="(min-width: 1000px)"/><img src="/local/front/images/methods-1.jpg" loading="lazy" decoding="async"/>
            </picture>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="methods-1">
        <div class="content">
          <h2>Стратегия лечения</h2>
          <div class="block-marked block-marked--white block-marked--bordered page-subsection">
            <h3><strong>Лечим иначе</strong></h3>
            <p>Наша методика <strong>принципиально отличается</strong> от классических методов лечения простатита.</p>
          </div>
          <div class="block-marked page-subsection">
            <p>На небактериальный хронический простатит приходится <strong>90% случаев</strong> всех диагнозов простатита.</p>
          </div>
          <div class="page-subsection">
            <p>В соответствии с нашим опытом в основе хронического небактериального простатита / синдрома хронической тазовой боли у мужчин лежат расстройства <strong>нейрососудистого происхождения</strong> вследствие воспаления в семявыводящих путях, которое приводит к фиброзу (соединительной ткани).</p>
            <p>При этом процессе воспаляется не сам семявыводящий проток или семенной пузырек, а ткани вокруг. Образуются <strong>инфильтраты</strong> — плотные образования, что-то типа фиброза, которые вызывают нарушение кровоснабжения тканей и нервных волокон.</p>
            <p>Этот процесс часто сопровождается болью разного характера и локализации.</p>
          </div>
          <div class="block-marked page-subsection">
            <p>По данным обследования сотен пациентов инфильтраты выявлялись <strong>в 93% случаев</strong> среди пациентов, которым исходно ставился диагноз СХТБ (синдром хронической тазовой боли или хронический небактериальный простатит).</p>
          </div>
          <div class="page-subsection">
            <p>Мы используем в основном <strong>физиотерапевтические методы лечения</strong>, направленные на исчезновение этих инфильтратов.</p>
            <p>В конце курса лечения, когда инфильтраты полностью убраны, иногда остаются <strong>слабыe мигрирующие боли</strong> в разных зонах, связанных через нервы.</p>
            <p>Дальнейшее лечение проходит с применением длинных <strong>акупунктурных игл</strong>.</p>
          </div>
          <div class="icon-block icon-block--no-drugs page-subsection">
            <div class="icon-block__inner">
              <p>Мы почти <strong>не используем фармакотерапию</strong>, так как она действует в основном на симптомы, а не на истинную причину заболевания.</p>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="methods-2">
        <div class="content">
          <h2>Преимущества нашей методики</h2>
          <div class="grid page-subsection">
            <div class="block-marked block-marked--bordered grid__cell grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue">Уникальность
              </p>
              <p>Не имеющая аналогов в мире методика лечения хронического простатита (синдрома хронической тазовой боли).</p>
            </div>
            <div class="block-marked block-marked--bordered grid__cell grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue">Эффективность
              </p>
              <p>После прохождения полного курса лечения 87% пациентов имеют стойкую ремиссию на протяжении 20 и более лет.</p>
            </div>
            <div class="block-marked block-marked--bordered grid__cell grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue">Проверенность
              </p>
              <p>Методика лечения используется с 1988 года. Проверена на сотнях пациентов. Методика зарегистрирована в институте Уронефрологии.</p>
            </div>
            <div class="block-marked block-marked--bordered grid__cell grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue">Безопасность
              </p>
              <p>Без операций, госпитализации, длительных курсов медикаментов, сложной диагностики и большого числа анализов.</p>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="methods-3">
        <div class="content">
          <h2>Процедуры</h2>
          <div class="icon-block icon-block--leader page-subsection">
            <div class="icon-block__inner">
              <p>Чтобы лечение было максимально эффективным, каждому пациенту мы предлагаем <strong>индивидуальный комплекс процедур.</strong></p>
            </div>
          </div>
          <div class="page-subsection">
            <picture>
              <source srcset="/local/front/images/methods-2_s.webp, /local/front/images/methods-2_s@2x.webp 2x" media="(max-width: 479px)"/>
              <source srcset="/local/front/images/methods-2_l.webp, /local/front/images/methods-2_l@2x.webp 2x" media="(min-width: 480px) and (max-width: 999px)"/>
              <source srcset="/local/front/images/methods-2_xl.webp, /local/front/images/methods-2_xl@2x.webp 2x" media="(min-width: 1000px)"/>
              <source srcset="/local/front/images/methods-2_s.jpg, /local/front/images/methods-2_s@2x.jpg 2x" media="(max-width: 479px)"/>
              <source srcset="/local/front/images/methods-2_l.jpg, /local/front/images/methods-2_l@2x.jpg 2x" media="(min-width: 480px) and (max-width: 999px)"/>
              <source srcset="/local/front/images/methods-2_xl.jpg, /local/front/images/methods-2_xl@2x.jpg 2x" media="(min-width: 1000px)"/><img src="/local/front/images/methods-2.jpg" loading="lazy" decoding="async"/>
            </picture>
          </div>
          <div class="grid page-subsection">
            <div class="block-marked block-marked--white grid__cell grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue">Пальцевой массаж
              </p>
              <p>Массаж помогает наладить кровообращение, избавиться от застойных явлений в железе, снизить тонус гладкой мускулатуры, улучшить обменные процессы.</p>
            </div>
            <div class="block-marked block-marked--white grid__cell grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue">Вибрационный массаж
              </p>
              <p>Мы используем авторскую методику массажа зоны проекции семявыводящих путей, которая позволяет проникнуть более глубоко чтобы достигнуть необходимой области.</p>
            </div>
            <div class="block-marked block-marked--white grid__cell">
              <p class="text-marked text-marked--size--xl text-marked--color--blue">Ударно волновая терапия
              </p>
              <p>За счет вибраций улучшается отток секрета, а также уменьшаются застойные явления.</p>
              <p>Применение ударно-волновой терапии с пальцевым трансректальным массажем инфильтратов, как бы обостряет процесс, вероятно, через травматическое воспаление, что сразу способствует более выраженному ответу на проводимый электрофорез.</p>
            </div>
            <div class="block-marked block-marked--white grid__cell grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue">Магнитотерапия
              </p>
              <p>Обеспечивает обезболивающее иммуномодулирующее и адаптогенное воздействие, нормализует обменные процессы.</p>
            </div>
            <div class="block-marked block-marked--white grid__cell grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue">Термотерапия
              </p>
              <p>Убирает оттек, обеспечивает сосудорасширяющий эффект, улучшение кровоснабжения, микроциркуляцию.</p>
            </div>
            <div class="block-marked block-marked--white grid__cell grid__cell--l--4 grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue">Электрофорез
              </p>
              <p>Локальный инъекционный электрофорез дает выраженный эффект с визуальным исчезновением инфильтратов (образований соединительной ткани).</p>
            </div>
            <div class="block-marked block-marked--white grid__cell grid__cell--l--4 grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue">Электростимуляция
              </p>
              <p>Способствует локальному усилению кровотока в семявыводящих путях и лучшему проникновению медикаментов, за счет расширения сосудов, улучшению питания клеток и тканей.</p>
            </div>
            <div class="block-marked block-marked--white grid__cell grid__cell--l--4 grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue">Аккупунктура
              </p>
              <p>Позволяет устранить остаточные тазовые боли с восстановлением нормального тонуса тазового дна и урогенитальной диафрагмы.</p>
            </div>
          </div>
          <div class="block-marked block-marked--bordered page-subsection">
            <p class="text-marked text-marked--size--xl text-marked--color--blue">Эффективное оборудование
            </p>
            <p>Используемый аппарат одобрен НИИ урологии им. Н.А. Лопаткина Минздрава России после <a href="https://smartprost.ru/wp-content/uploads/2019/01/unnamed-file.pdf" target="_blank">клинических испытаний</a>, которые доказали, что эффективность лечения повышается в среднем на 42%.</p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top home-treatment" id="methods-4">
        <div class="content">
          <h2>Как попасть на лечение?</h2>
          <div class="slider page-subsection">
            <div class="slider__slides swiper">
              <div class="slider__wrapper swiper-wrapper">
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Консультация
                    </p>
                    <div class="home-treatment-item__tel">По телефону: <span>+7 (495) 204-14-94</span></div>
                    <div class="home-treatment-item__text">Медицинский консультант расспросит о симптомах, подберет подходящего врача, расскажет про стоимость процедур и запишет на первичный прием.</div>
                    <div class="home-treatment-item__duration">5 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Прием специалиста
                    </p>
                    <div class="home-treatment-item__text">Врач внимательно и спокойно выслушает вас, изучит данные предыдущих исследований, назначит только необходимую диагностику и поставит предварительный диагноз.</div>
                    <div class="home-treatment-item__duration">от 30 до 40 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Диагностика в тот же день
                    </p>
                    <div class="home-treatment-item__text">Врач выполнит осмотр предстательной железы и семявыводящих путей, возьмет биоматериал для анализов. <br> На основе полученных данных врач сможет назначить индивидуальный курс лечения.</div>
                    <div class="home-treatment-item__duration">от 10 до 20 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Повторный прием и лечение
                    </p>
                    <div class="home-treatment-item__text">Врач подробно расскажет о результатах анализа, детально объяснит суть курса лечения, опишет процедуры и препараты. <br> В этот же день вы сможете начать лечение.</div>
                    <div class="home-treatment-item__duration">от 20 до 40 минут</div>
                  </div>
                </div>
              </div>
              <div class="slider__arrow slider__arrow--prev"></div>
              <div class="slider__arrow slider__arrow--next"></div>
              <div class="slider__pagination"></div>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="methods-5">
        <div class="content">
          <h2>Цены</h2>
          <div class="page-subsection">
            <div class="table-wrap">
              <table class="table">
                <colgroup class="table__colgroup table__colgroup--2">
                  <col class="table__col"/>
                  <col class="table__col"/>
                </colgroup>
                <tr class="table__row">
                  <td class="table__cell">Прием (осмотр консультация) врача-уролога первичный
                  </td>
                  <td class="table__cell text-align-center">3500 руб.
                  </td>
                </tr>
                <tr class="table__row">
                  <td class="table__cell">Прием (осмотр консультация) врача-уролога вторичный
                  </td>
                  <td class="table__cell text-align-center">2500 руб.
                  </td>
                </tr>
              </table>
            </div>
            <p class="text-marked text-marked--size--s text-marked--color--grey">Указанные на сайте цены не являются публичной офертой. Стоимость услуг зависит от уровня квалификации специалиста.
            </p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="methods-6">
        <div class="content">
          <h2>Частые вопросы</h2>
          <div class="page-subsection grid">
            <div class="grid__cell grid__cell--m--9 grid__cell--xs--12">
              <div class="accordion-item par">
                <div class="accordion-item__header">Как долго длится лечение?
                  <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                      <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                      <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                    </path>
                  </svg>
                </div>
                <div class="accordion-item__body">
                  <p>Длительность лечения зависит от&nbsp;времени, которое вы&nbsp;болеете, а&nbsp;также от&nbsp;реакции организма на&nbsp;процесс лечения.</p>
                </div>
              </div>
              <div class="accordion-item par">
                <div class="accordion-item__header">Какие методы вы&nbsp;используете для лечения?
                  <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                      <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                      <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                    </path>
                  </svg>
                </div>
                <div class="accordion-item__body">
                  <p>Мы&nbsp;используем в&nbsp;основном физиотерапевтические методы лечения. Но&nbsp;область воздействия, а&nbsp;также способ принципиально отличается от&nbsp;обычных методов лечения хронического простатита.</p>
                </div>
              </div>
              <div class="accordion-item par">
                <div class="accordion-item__header">Почему я&nbsp;раньше не&nbsp;слышал о&nbsp;данной методике?
                  <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                      <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                      <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                    </path>
                  </svg>
                </div>
                <div class="accordion-item__body">
                  <p>Долгое время данная методика применялась только в&nbsp;одной клинике в&nbsp;стране и&nbsp;никак не&nbsp;рекламировалась. Информация передавалась по&nbsp;&laquo;сарафанному радио&raquo; от&nbsp;пациента к&nbsp;пациенту.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <footer class="container page-section page-section--background-color footer">
      <div class="content footer__inner">
        <div class="footer__row footer__row--name">
          <div class="footer__name">Инамед <span>Клиника урологии</span></div>
          <div class="footer__slogan">Лечим иначе!</div>
        </div>
        <!-- <div class="footer__row footer__row--license footer__item footer__item--license">
          a(href='#') Лицензии и сертификаты
        </div> -->
        <div class="footer__row footer__row--copyright">
          <div class="footer__copyright">
            <p class="footer__year">2024, ООО &laquo;Инамед&raquo;</p><a href="#">Правила использования сайтом</a><a href="#">Соглашение на обработку персональных данных</a>
          </div>
          <div class="footer__info">Имеются противопоказания, необходима консультация специалиста.</div>
          <div class="footer__info" style="display: block">Наша клиника располагается на базе сети диагностических центров <a href="https://gorlab.ru/" target="_blank">«Горлаб»</a></div>
        </div>
      </div>
    </footer>
  </body>
  <link rel="stylesheet" href="/local/front/template/styles/style.rest.css?v=<?php require __DIR__ . '/version.php'?>"/>
  <link rel="stylesheet" href="/local/front/template/styles/methods.rest.css?v=<?php require __DIR__ . '/version.php'?>"/>
  <script src="/local/front/template/scripts/script.js?v=<?php require __DIR__ . '/version.php'?>"></script>
  <script src="/local/front/template/scripts/methods.js?v=<?php require __DIR__ . '/version.php'?>"></script>
</html>
