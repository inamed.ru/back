<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Контакты урологической клиники в Москве &#34;Инамед&#34;: современные методы лечения простатита, синдрома хронической тазовой боли, высококвалифицированные специалисты, комплексная диагностика. Запись на прием по телефону">
    <link rel="stylesheet" href="/local/front/template/styles/style.core.css?v=<?php require __DIR__ . '/version.php'?>"/>
    <link rel="stylesheet" href="/local/front/template/styles/contacts.core.css?v=<?php require __DIR__ . '/version.php'?>"/>
    <title>Контакты урологической клиники "Инамед"</title>
      <!-- Метрика -->
      <?php require __DIR__ . '/includes/metrika.php' ?>
  </head>
  <body>
    <header class="header">
      <div class="container header__inner">
        <div class="content">
          <a class="logo header__logo" href="/"></a>
            <div class="header__name"> <a href="/">Инамед </a><span>Клиника урологии</span></div><a class="header__call" href="tel:+74952041494" aria-label="Позвонить"></a>
            <button class="header__switcher js-header-switcher hidden-l" type="button" aria-label="Открыть или закрыть мобильное меню"></button>
            <?php require __DIR__ . '/includes/menu.php'?>

        </div>
      </div>
      <div class="container header__bottom">
        <div class="content">
          <p class="text-marked text-marked--size--l text-align-center">Излечиваем хронический простатит (СХТБ) с <strong>эффективностью 87%</strong>.
          </p>
        </div>
      </div>
    </header>
    <main>
      <div class="container breadcrumbs">
        <div class="content breadcrumbs__inner"><a class="breadcrumbs__parent" href="/">Главная</a>
          <div class="breadcrumbs__current">Контакты</div>
        </div>
      </div>
      <section class="container page-section">
        <div class="content">
          <h1>Контакты</h1>
          <dl class="contacts page-subsection block-marked block-marked--white">
            <dt class="contacts__name contacts__name--address">Адрес</dt>
            <dd class="contacts__value">Москва, ул. Образцова,  д. 14</dd>
            <dt class="contacts__name contacts__name--metro">Метро</dt>
            <dd class="contacts__value">Менделеевская / Марьина Роща</dd>
            <dt class="contacts__name contacts__name--schedule">Время работы</dt>
            <dd class="contacts__value">ежедневно с 9:00 до 21:00</dd>
            <dt class="contacts__name contacts__name--tel">Телефон</dt>
            <dd class="contacts__value">+7 (495) 204-14-94</dd>
            <dt class="contacts__name contacts__name--email">E-mail</dt>
            <dd class="contacts__value">ask@inamed.ru</dd>
          </dl>
        </div>
      </section>
      <section class="container page-section">
        <div class="content">
          <h2>Карта проезда</h2><div style="position:relative;overflow:hidden;" class="page-subsection">
<a href="https://yandex.ru/maps/213/moscow/?utm_medium=mapframe&utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:0px;">Москва</a>
<a href="https://yandex.ru/maps/213/moscow/house/ulitsa_obraztsova_14/Z04YcAZgSEcCQFtvfXt5dnVjZg==/?ll=37.610663%2C55.788859&utm_medium=mapframe&utm_source=maps&z=13.96" style="color:#eee;font-size:12px;position:absolute;top:14px;">Улица Образцова, 14 — Яндекс Карты</a>
<iframe src="https://yandex.ru/map-widget/v1/?ll=37.615596%2C55.787710&mode=search&ol=geo&ouri=ymapsbm1%3A%2F%2Fgeo%3Fdata%3DCgg1Njc0MzU0ORI90KDQvtGB0YHQuNGPLCDQnNC-0YHQutCy0LAsINGD0LvQuNGG0LAg0J7QsdGA0LDQt9GG0L7QstCwLCAxNCIKDcRwFkIV0CZfQg%2C%2C&z=14.39" width="560" height="400" frameborder="1" allowfullscreen="true" style="position:relative; width: 100%; height: min(675px, 50vh)"></iframe>
</div>
        </div>
      </section>
      <section class="container page-section">
        <div class="content">
          <h2>Наша клиника</h2>
          <div class="page-subsection">
            <p>
              <picture>
                <source srcset="/local/front/images/contacts-1.webp"/>
                <source srcset="/local/front/images/contacts-1.jpg"/><img src="/local/front/images/contacts-1.jpg" loading="lazy" decoding="async" style="width: min(100%, 800px - var(--base-padding));"/>
              </picture>
            </p>
            <p><i>Наша клиника располагается на базе сети диагностических центров <strong>«Горлаб».</strong></i></p>
          </div>
        </div>
      </section>
      <section class="container page-section">
        <div class="content">
          <h2>Фото клиники</h2>
          <div class="slider home-gallery page-subsection">
            <div class="slider__slides swiper">
              <div class="slider__wrapper swiper-wrapper">
                <div class="slide swiper-slide">
                  <picture>
                    <source srcset="/local/front/images/photo-1.webp, /local/front/images/photo-1@2x.webp 2x"/>
                    <source srcset="/local/front/images/photo-1.jpg, /local/front/images/photo-1@2x.jpg 2x"/><img class="image-border-radius-s" src="/local/front/images/photo-1.jpg" loading="lazy" decoding="async"/>
                  </picture>
                </div>
                <div class="slide swiper-slide">
                  <picture>
                    <source srcset="/local/front/images/photo-2.webp, /local/front/images/photo-2@2x.webp 2x"/>
                    <source srcset="/local/front/images/photo-2.jpg, /local/front/images/photo-2@2x.jpg 2x"/><img class="image-border-radius-s" src="/local/front/images/photo-2.jpg" loading="lazy" decoding="async"/>
                  </picture>
                </div>
                <div class="slide swiper-slide">
                  <picture>
                    <source srcset="/local/front/images/photo-3.webp, /local/front/images/photo-3@2x.webp 2x"/>
                    <source srcset="/local/front/images/photo-3.jpg, /local/front/images/photo-3@2x.jpg 2x"/><img class="image-border-radius-s" src="/local/front/images/photo-3.jpg" loading="lazy" decoding="async"/>
                  </picture>
                </div>
              </div>
              <div class="slider__arrow slider__arrow--prev"></div>
              <div class="slider__arrow slider__arrow--next"></div>
              <div class="slider__pagination"></div>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="callback-form">
        <div class="content">
          <h2>Запишитесь на прием сегодня</h2>
          <div class="page-subsection block-marked">
            <p>Администратор свяжется с вами для уточнения даты и времени.</p>
            <form class="grid grid--small-gap--y grid--justify--center grid--align-items--flex-end form par" method="post" action="" data-answer-success='Спасибо за запись! Наш администратор свяжется с вами в ближайшее время для уточнения даты и времени.'>
              <label class="form-input form-input--text grid__cell grid__cell--m--4 grid__cell--s--6 form__input">
                <span class="form-input__label"><strong>Имя *</strong>
                </span>
                <input class="form-input__field" type="text" name="name" required="required" placeholder="Имя" checked="checked"></input>
              </label>
              <label class="form-input form-input--tel grid__cell grid__cell--m--4 grid__cell--s--6 form__input">
                <span class="form-input__label"><strong>Телефон *</strong>
                </span>
                <input class="form-input__field" type="tel" name="tel" required="required" placeholder="+7 (9__) ___-__-__" checked="checked"></input>
              </label>
              <button class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--8 button form__submit" type="submit">Отправить</button>
              <label class="form-input form-input--checkbox grid__cell form__agreement">
                <span class="form-input__label">Отправляя эту форму, вы соглашаетесь с условиями
                  <a class="link" href="#">Обработки персональных данных.
                  </a>
                </span>
                <input class="form-input__field" type="checkbox" required="required" checked="checked"></input>
              </label>
            </form>
          </div>
        </div>
      </section>
    </main>
    <footer class="container page-section page-section--background-color footer">
      <div class="content footer__inner">
        <div class="footer__row footer__row--name">
          <div class="footer__name">Инамед <span>Клиника урологии</span></div>
          <div class="footer__slogan">Лечим иначе!</div>
        </div>
        <!-- <div class="footer__row footer__row--license footer__item footer__item--license">
          a(href='#') Лицензии и сертификаты
        </div> -->
        <div class="footer__row footer__row--copyright">
          <div class="footer__copyright">
            <p class="footer__year">2024, ООО &laquo;Инамед&raquo;</p><a href="#">Правила использования сайтом</a><a href="#">Соглашение на обработку персональных данных</a>
          </div>
          <div class="footer__info">Имеются противопоказания, необходима консультация специалиста.</div>
          <div class="footer__info" style="display: block">Наша клиника располагается на базе сети диагностических центров <a href="https://gorlab.ru/" target="_blank">«Горлаб»</a></div>
        </div>
      </div>
    </footer>
  </body>
  <link rel="stylesheet" href="/local/front/template/styles/style.rest.css?v=<?php require __DIR__ . '/version.php'?>"/>
  <link rel="stylesheet" href="/local/front/template/styles/contacts.rest.css?v=<?php require __DIR__ . '/version.php'?>"/>
  <script src="/local/front/template/scripts/script.js?v=<?php require __DIR__ . '/version.php'?>"></script>
  <script src="/local/front/template/scripts/contacts.js?v=<?php require __DIR__ . '/version.php'?>"></script>
</html>
