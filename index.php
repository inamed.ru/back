<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Клиника урологии в Москве - урологический центр "Инамед"</title>
    <meta name="description" content="Урологическая клиника в Москве &#34;Инамед&#34;: современные методы лечения простатита, синдрома хронической тазовой боли, высококвалифицированные специалисты, комплексная диагностика. Запись на прием по телефону">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="/local/front/template/styles/style.core.css?v=<?php require __DIR__ . '/version.php'?>"/>
    <link rel="stylesheet" href="/local/front/template/styles/main.core.css?v=<?php require __DIR__ . '/version.php'?>"/>
      <!-- Метрика -->
      <?php require __DIR__ . '/includes/metrika.php' ?>
  </head>
  <body>
    <header class="header">
      <div class="container header__inner">
        <div class="content">
          <a class="logo header__logo"></a>
            <div class="header__name"> <a href="/">Инамед </a><span>Клиника урологии</span></div><a class="header__call" href="tel:+74952041494" aria-label="Позвонить"></a>
          <button class="header__switcher js-header-switcher hidden-l" type="button" aria-label="Открыть или закрыть мобильное меню"></button>
         <?php require __DIR__ . '/includes/menu.php'?>
        </div>
      </div>
    </header>
    <main>
      <section class="container page-section page-section--no-padding--top page-section--no-padding--bottom home-header">
        <picture>
          <source srcset="/local/front/images/home-header_s.webp, /local/front/images/home-header_s@2x.webp 2x" media="(max-width: 479px)"/>
          <source srcset="/local/front/images/home-header_m.webp, /local/front/images/home-header_m@2x.webp 2x" media="(min-width: 480px) and (max-width: 799px)"/>
          <source srcset="/local/front/images/home-header_l.webp, /local/front/images/home-header_l@2x.webp 2x" media="(min-width: 800px)"/>
          <source srcset="/local/front/images/home-header_s.jpg, /local/front/images/home-header_s@2x.jpg 2x" media="(max-width: 479px)"/>
          <source srcset="/local/front/images/home-header_m.jpg, /local/front/images/home-header_m@2x.jpg 2x" media="(min-width: 480px) and (max-width: 799px)"/>
          <source srcset="/local/front/images/home-header_l.jpg, /local/front/images/home-header_l@2x.jpg 2x" media="(min-width: 800px)"/><img class="home-header__image" src="/local/front/images/home-header.jpg" loading="" decoding="async" width="417" height="289" fetchpriority="high"/>
        </picture>
        <div class="content home-header__content">
          <h1 class="home-header__title">Излечиваем хронический простатит (СХТБ) с&nbsp;<strong>эффективностью 87%</strong></h1><a class="button" href="#callback-form">Записаться на консультацию</a>
        </div>
      </section>
      <section class="container page-section">
        <div class="content">
          <h2>Наши преимущества</h2>
          <div class="grid page-subsection">
            <div class="block-marked block-marked--bordered grid__cell grid__cell--l--4 grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue"><strong>Эффективная методика</strong>
              </p>
              <p><strong>Отличная от других</strong> методика. Единственная, которая направлена на <strong>истинную причину болезни</strong>, а не на симптомы.  </p>
            </div>
            <div class="block-marked block-marked--bordered grid__cell grid__cell--l--4 grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue"><strong>Специализация</strong>
              </p>
              <p>Сконцентрировали компетенции в одной сфере. Глубоко понимаем <strong>причины заболевания</strong>.  </p>
            </div>
            <div class="block-marked block-marked--bordered grid__cell grid__cell--l--4 grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue"><strong>Внимание к вашей проблеме</strong>
              </p>
              <p>Лучше других <strong>понимаем пациентов</strong> с хроническим простатитом.  </p>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--background-color">
        <div class="content">
          <h2>Когда вам нужно обратиться именно к нам?</h2>
          <div class="grid page-subsection">
            <div class="grid__cell grid__cell--m--6 grid__cell--xs--12">
              <picture>
                <source srcset="/images/main-1.webp"/>
                <source srcset="/images/main-1.png"/><img src="/images/main-1.png" loading="lazy" decoding="async" alt="Когда вам нужно обратиться именно к нам?"/>
              </picture>
            </div>
            <div class="grid__cell grid__cell--m--6 grid__cell--xs--12">
              <ul class="unordered-list unordered-list--marked">
                <li>
                  <p>Вы&nbsp;<strong>обошли несколько врачей</strong> и&nbsp;везде ставят диагноз &laquo;хронический простатит&raquo;.</p>
                </li>
                <li>
                  <p>Перепробовали <strong>несколько вариантов лечения</strong>, но&nbsp;ничего сильно не&nbsp;изменило ваше состояние.</p>
                </li>
                <li>
                  <p>Сдали <strong>множество анализов</strong>, пытаясь найти инфекции или бактерии. Принимали курсы антибиотиков, которые не&nbsp;дали результатов.</p>
                </li>
                <li>
                  <p>Никто из&nbsp;врачей не&nbsp;<strong>может четко объяснить</strong>, что с&nbsp;вами происходит и&nbsp;как это вылечить.</p>
                </li>
                <li>
                  <p>Столкнулись с&nbsp;хамством, <strong>безразличием врачей</strong>, &laquo;выкачиванием&raquo; денег.</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section">
        <div class="content">
          <h2>О методике</h2>
          <div class="grid page-subsection">
            <div class="block-marked block-marked--bordered grid__cell grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue"><strong>Уникальность</strong>
              </p>
              <p>Не&nbsp;имеющая аналогов в&nbsp;мире методика лечения хронического простатита (синдрома хронической тазовой боли).  </p>
            </div>
            <div class="block-marked block-marked--bordered grid__cell grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue"><strong>Эффективность</strong>
              </p>
              <p>После прохождения полного курса лечения&nbsp;87% пациентов имеют стойкую ремиссию на&nbsp;протяжении 20&nbsp;и&nbsp;более лет.  </p>
            </div>
            <div class="block-marked block-marked--bordered grid__cell grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue"><strong>Проверенность</strong>
              </p>
              <p>Методика лечения используется с&nbsp;1988&nbsp;года. Проверена на&nbsp;сотнях пациентов.  </p>
            </div>
            <div class="block-marked block-marked--bordered grid__cell grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue"><strong>Безопасность</strong>
              </p>
              <p>Без операций, госпитализации, длительных курсов медикаментов, сложной диагностики и&nbsp;большого числа анализов.</p>
            </div>
          </div>
          <div class="page-subsection">
            <div class="link-item link-item--info"><a class="link-item__header" href="/metody-lecheniya/">Получите информацию о методе лечения</a>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--background-color">
        <div class="content">
          <h2>Остались вопросы?</h2>
          <div class="page-subsection par">
            <p>Свяжитесь с нами и <strong>узнайте подробности</strong> о заболевании, методике лечения, ценах.</p>
          </div>
          <div class="grid grid--justify--center par">
            <a class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--8 button par" href="tel:+74952041494">Позвонить</a>
          </div>
        </div>
      </section>
      <section class="container page-section">
        <div class="content">
          <h2>Когда записаться в&nbsp;нашу клинику?</h2>
          <div class="grid page-subsection">
            <div class="grid__cell grid__cell--m--6 grid__cell--xs--12">
              <picture>
                <source srcset="/images/main-2.webp"/>
                <source srcset="/images/main-2.png"/><img src="/images/main-2.png" loading="lazy" decoding="async" alt="Когда записаться в нашу клинику?"/>
              </picture>
            </div>
            <div class="grid__cell grid__cell--m--6 grid__cell--xs--12">
              <p>Если вас беспокоят один или несколько таких симптомов, вам <strong>необходима консультация</strong> нашего уролога:</p>
              <ul class="unordered-list unordered-list--marked par">
                <li>
                  <p>Боль в&nbsp;паховой области, которая может отдавать в&nbsp;яичко, половой член, внутреннюю поверхность бедра, копчик, задний проход</p>
                </li>
                <li>
                  <p>Затрудненное или учащенное мочеиспускание, неудержимые позывы</p>
                </li>
                <li>
                  <p>Снижение эрекции</p>
                </li>
                <li>
                  <p>Боли усиливаются после эякуляции или мочеиспускания</p>
                </li>
                <li>
                  <p>Наличие желтой слизи в&nbsp;эякуляте</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top">
        <div class="content">
          <h2>Цены</h2>
          <div class="page-subsection">
            <div class="table-wrap">
              <table class="table">
                <colgroup class="table__colgroup table__colgroup--2">
                  <col class="table__col"/>
                  <col class="table__col"/>
                </colgroup>
                <tr class="table__row">
                  <td class="table__cell">Прием (осмотр консультация) врача-уролога первичный
                  </td>
                  <td class="table__cell text-align-center">3500 руб.
                  </td>
                </tr>
                <tr class="table__row">
                  <td class="table__cell">Прием (осмотр консультация) врача-уролога вторичный
                  </td>
                  <td class="table__cell text-align-center">2500 руб.
                  </td>
                </tr>
              </table>
            </div>
            <p class="text-marked text-marked--size--s text-marked--color--grey">Указанные на сайте цены не являются публичной офертой. Стоимость услуг зависит от уровня квалификации специалиста.
            </p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top">
        <div class="content">
          <h2>Частые вопросы</h2>
          <div class="page-subsection grid">
            <div class="grid__cell grid__cell--m--9 grid__cell--xs--12">
              <div class="accordion-item par">
                <div class="accordion-item__header">Почему я&nbsp;могу вам доверять?
                  <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                      <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                      <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                    </path>
                  </svg>
                </div>
                <div class="accordion-item__body">
                  <p>Методика лечения используется с&nbsp;1988&nbsp;года. Проверена на&nbsp;сотнях пациентов. Методика зарегистрирована в&nbsp;институте Уронефрологии им. Сеченова.</p>
                </div>
              </div>
              <div class="accordion-item par">
                <div class="accordion-item__header">Какие методы вы&nbsp;используете для лечения?
                  <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                      <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                      <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                    </path>
                  </svg>
                </div>
                <div class="accordion-item__body">
                  <p>Мы&nbsp;используем в&nbsp;основном физиотерапевтические методы лечения. Но&nbsp;область воздействия, а&nbsp;также способ принципиально отличается от&nbsp;обычных методов лечения хронического простатита.</p>
                </div>
              </div>
              <div class="accordion-item par">
                <div class="accordion-item__header">В&nbsp;чем особенность вашей методики?
                  <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                      <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                      <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                    </path>
                  </svg>
                </div>
                <div class="accordion-item__body">
                  <p>В&nbsp;соответствии с&nbsp;нашим опытом в&nbsp;основе хронического простатита&nbsp;/ синдрома хронической тазовой боли у&nbsp;мужчин лежат расстройства нейрососудистого происхождения вследствие воспаления в&nbsp;семявыводящих путях, которое приводит к&nbsp;фиброзу.</p>
                  <p>При этом процессе воспаляется не&nbsp;сам семявыводящий проток или семенной пузырек, а&nbsp;ткани вокруг. Образуются инфильтраты&nbsp;&mdash; плотные образования, что-то типа фиброза с&nbsp;развитием ишемии.</p>
                  <p>Мы&nbsp;используем в&nbsp;основном физиотерапевтические методы лечения, направленные на&nbsp;разрешение этих инфильтратов.</p>
                </div>
              </div>
              <div class="accordion-item par">
                <div class="accordion-item__header">Почему я&nbsp;раньше не&nbsp;слышал о&nbsp;данной методике?
                  <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                      <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                      <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                    </path>
                  </svg>
                </div>
                <div class="accordion-item__body">
                  <p>Долгое время данная методика применялась только в&nbsp;одной клинике в&nbsp;стране и&nbsp;никак не&nbsp;рекламировалась. Информация передавалась по&nbsp;&laquo;сарафанному радио&raquo; от&nbsp;пациента к&nbsp;пациенту.</p>
                </div>
              </div>
              <div class="accordion-item par">
                <div class="accordion-item__header">Излечим&nbsp;ли хронический простатит?
                  <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                      <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                      <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                    </path>
                  </svg>
                </div>
                <div class="accordion-item__body">
                  <p>Да, наш опыт однозначно доказывает, что после курса лечения пациент может полностью забыть про заболевание и&nbsp;иметь стойкую ремиссию на&nbsp;протяжении 20&nbsp;и&nbsp;более лет.</p>
                </div>
              </div>
              <div class="accordion-item par">
                <div class="accordion-item__header">Болезненно&nbsp;ли лечение?
                  <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                      <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                      <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                    </path>
                  </svg>
                </div>
                <div class="accordion-item__body">
                  <p>Мы&nbsp;стремимся чтобы процедуры проходили с&nbsp;минимальным дискомфортом для пациента.</p>
                </div>
              </div>
              <div class="accordion-item par">
                <div class="accordion-item__header">Как долго длится лечение?
                  <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                      <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                      <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                    </path>
                  </svg>
                </div>
                <div class="accordion-item__body">
                  <p>Длительность лечения зависит от&nbsp;времени, которое вы&nbsp;болеете, а&nbsp;также от&nbsp;реакции организма на&nbsp;процесс лечения.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--background-color">
        <div class="content">
          <h2>О клинике</h2>
          <div class="block-marked block-marked--white page-subsection">
            <p>Наша клиника&nbsp;&mdash; центр компетенций в&nbsp;лечении хронического простатита (синдрома хронической тазовой боли).</p>
          </div>
          <div class="page-subsection">
            <p>Мы&nbsp;<strong>специализированная клиника</strong>&nbsp;&mdash; сконцентрировали компетенции в&nbsp;одной сфере, в&nbsp;которой хорошо разбираемся. Глубокое <strong>понимание причин заболевания</strong>, состояния пациента и&nbsp;подхода к&nbsp;лечению.</p>
            <p>Используем <strong>накопленный за&nbsp;30-лет опыт</strong> в&nbsp;лечении хронического простатита (синдрома хронической тазовой боли).</p>
          </div>
        </div>
      </section>
      <section class="container page-section">
        <div class="content">
          <h2>Фото клиники</h2>
          <div class="slider home-gallery page-subsection">
            <div class="slider__slides swiper">
              <div class="slider__wrapper swiper-wrapper">
                <div class="slide swiper-slide">
                  <picture>
                    <source srcset="/local/front/images/photo-1.webp, /local/front/images/photo-1@2x.webp 2x"/>
                    <source srcset="/local/front/images/photo-1.jpg, /local/front/images/photo-1@2x.jpg 2x"/><img class="image-border-radius-s" src="/local/front/images/photo-1.jpg" loading="lazy" decoding="async"/>
                  </picture>
                </div>
                <div class="slide swiper-slide">
                  <picture>
                    <source srcset="/local/front/images/photo-2.webp, /local/front/images/photo-2@2x.webp 2x"/>
                    <source srcset="/local/front/images/photo-2.jpg, /local/front/images/photo-2@2x.jpg 2x"/><img class="image-border-radius-s" src="/local/front/images/photo-2.jpg" loading="lazy" decoding="async"/>
                  </picture>
                </div>
                <div class="slide swiper-slide">
                  <picture>
                    <source srcset="/local/front/images/photo-3.webp, /local/front/images/photo-3@2x.webp 2x"/>
                    <source srcset="/local/front/images/photo-3.jpg, /local/front/images/photo-3@2x.jpg 2x"/><img class="image-border-radius-s" src="/local/front/images/photo-3.jpg" loading="lazy" decoding="async"/>
                  </picture>
                </div>
              </div>
              <div class="slider__arrow slider__arrow--prev"></div>
              <div class="slider__arrow slider__arrow--next"></div>
              <div class="slider__pagination"></div>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top home-treatment">
        <div class="content">
          <h2>Как попасть на лечение?</h2>
          <div class="slider page-subsection">
            <div class="slider__slides swiper">
              <div class="slider__wrapper swiper-wrapper">
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Консультация
                    </p>
                    <div class="home-treatment-item__tel">По телефону: <span>+7 (495) 204-14-94</span></div>
                    <div class="home-treatment-item__text">Медицинский консультант расспросит о симптомах, подберет подходящего врача, расскажет про стоимость процедур и запишет на первичный прием.</div>
                    <div class="home-treatment-item__duration">5 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Прием специалиста
                    </p>
                    <div class="home-treatment-item__text">Врач внимательно и спокойно выслушает вас, изучит данные предыдущих исследований, назначит только необходимую диагностику и поставит предварительный диагноз.</div>
                    <div class="home-treatment-item__duration">от 30 до 40 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Диагностика в тот же день
                    </p>
                    <div class="home-treatment-item__text">Врач выполнит осмотр предстательной железы и семявыводящих путей, возьмет биоматериал для анализов. <br> На основе полученных данных врач сможет назначить индивидуальный курс лечения.</div>
                    <div class="home-treatment-item__duration">от 10 до 20 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Повторный прием и лечение
                    </p>
                    <div class="home-treatment-item__text">Врач подробно расскажет о результатах анализа, детально объяснит суть курса лечения, опишет процедуры и препараты. <br> В этот же день вы сможете начать лечение.</div>
                    <div class="home-treatment-item__duration">от 20 до 40 минут</div>
                  </div>
                </div>
              </div>
              <div class="slider__arrow slider__arrow--prev"></div>
              <div class="slider__arrow slider__arrow--next"></div>
              <div class="slider__pagination"></div>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="callback-form">
        <div class="content">
          <h2>Запишитесь на прием сегодня</h2>
          <div class="page-subsection block-marked">
            <p>Администратор свяжется с вами для уточнения даты и времени.</p>
            <form class="grid grid--small-gap--y grid--justify--center grid--align-items--flex-end form par" method="post" action="" data-answer-success='Спасибо за запись! Наш администратор свяжется с вами в ближайшее время для уточнения даты и времени.'>
              <label class="form-input form-input--text grid__cell grid__cell--m--4 grid__cell--s--6 form__input">
                <span class="form-input__label"><strong>Имя *</strong>
                </span>
                <input class="form-input__field" type="text" name="name" required="required" placeholder="Имя" checked="checked"></input>
              </label>
              <label class="form-input form-input--tel grid__cell grid__cell--m--4 grid__cell--s--6 form__input">
                <span class="form-input__label"><strong>Телефон *</strong>
                </span>
                <input class="form-input__field" type="tel" name="tel" required="required" placeholder="+7 (9__) ___-__-__" checked="checked"></input>
              </label>
              <button class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--8 button form__submit" type="submit">Отправить</button>
              <label class="form-input form-input--checkbox grid__cell form__agreement">
                <span class="form-input__label">Отправляя эту форму, вы соглашаетесь с условиями
                  <a class="link" href="#">Обработки персональных данных.
                  </a>
                </span>
                <input class="form-input__field" type="checkbox" required="required" checked="checked"></input>
              </label>
            </form>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top grid home-contacts">
        <div class="content">
          <div class="grid__cell grid__cell--m--6 grid__cell--xs--12">
            <div class="home-contacts__inner">
              <h2>Наш адрес</h2>
              <p class="text-marked text-marked--size--l">г. Москва, ул.&nbsp;Образцова, д.&nbsp;14
              </p>
              <div class="home-contacts__icon home-contacts__icon--call"><a class="link" href="tel:+74952041494">+7 (495) 204-14-94</a></div>
              <div class="home-contacts__icon home-contacts__icon--mail"><a class="link" href="mailto:ask@inamed.ru">ask@inamed.ru</a></div>
            </div>
            <div class="home-contacts__map" id="home-contacts-map"></div>
          </div>
        </div>
      </section>
    </main>
    <footer class="container page-section page-section--background-color footer">
      <div class="content footer__inner">
        <div class="footer__row footer__row--name">
          <div class="footer__name">Инамед <span>Клиника урологии</span></div>
          <div class="footer__slogan">Лечим иначе!</div>
        </div>

        <!--.footer__row.footer__row--license.footer__item.footer__item--license
        // a(href='#') Лицензии и сертификаты
        -->

        <!-- <div class="footer__row footer__row--license footer__item footer__item--license">
          a(href='#') Лицензии и сертификаты
        </div> -->

        <div class="footer__row footer__row--copyright">
          <div class="footer__copyright">
            <p class="footer__year">2024, ООО &laquo;Инамед&raquo;</p><a href="#">Правила использования сайтом</a><a href="#">Соглашение на обработку персональных данных</a>
          </div>
          <div class="footer__info">Имеются противопоказания, необходима консультация специалиста.</div>
          <div class="footer__info" style="display: block">Наша клиника располагается на базе сети диагностических центров <a href="https://gorlab.ru/" target="_blank">«Горлаб»</a></div>
        </div>
      </div>
    </footer>
  </body>
  <link rel="stylesheet" href="/local/front/template/styles/style.rest.css?v=<?php require __DIR__ . '/version.php'?>"/>
  <link rel="stylesheet" href="/local/front/template/styles/main.rest.css?v=<?php require __DIR__ . '/version.php'?>"/>
  <script src="/local/front/template/scripts/script.js?v=<?php require __DIR__ . '/version.php'?>"></script>
  <script src="/local/front/template/scripts/main.js?v=<?php require __DIR__ . '/version.php'?>"></script>
</html>
