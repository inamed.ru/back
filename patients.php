<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Информация для пациентов клиники &#34;Инамед&#34;: современные методы лечения простатита, синдрома хронической тазовой боли, высококвалифицированные специалисты, комплексная диагностика. Запись на прием по телефону">
    <link rel="stylesheet" href="/local/front/template/styles/style.core.css?v<?php require __DIR__ . '/version.php'?>"/>
    <link rel="stylesheet" href="/local/front/template/styles/methods.core.css?v<?php require __DIR__ . '/version.php'?>"/>
    <title>Информация для пациентов клиники "Инамед"</title>
      <!-- Метрика -->
      <?php require __DIR__ . '/includes/metrika.php' ?>
  </head>
  <body>
    <header class="header">
      <div class="container header__inner">
        <div class="content">
          <a class="logo header__logo" href="/"></a>
            <div class="header__name"> <a href="/">Инамед </a><span>Клиника урологии</span></div><a class="header__call" href="tel:+74952041494" aria-label="Позвонить"></a>
            <button class="header__switcher js-header-switcher hidden-l" type="button" aria-label="Открыть или закрыть мобильное меню"></button>
            <?php require __DIR__ . '/includes/menu.php'?>
        </div>
      </div>
      <div class="container header__bottom">
        <div class="content">
          <p class="text-marked text-marked--size--l text-align-center">Излечиваем хронический простатит (СХТБ) с <strong>эффективностью 87%</strong>.
          </p>
        </div>
      </div>
    </header>
    <main>
      <div class="container breadcrumbs">
        <div class="content breadcrumbs__inner"><a class="breadcrumbs__parent" href="/">Главная</a>
          <div class="breadcrumbs__current">Пациентам</div>
        </div>
      </div>
      <section class="container page-section">
        <div class="content">
          <h1>Пациентам</h1>
          <div class="accordion-item page-subsection">
            <div class="accordion-item__header">Оглавление
              <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                  <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                  <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                </path>
              </svg>
            </div>
            <div class="accordion-item__body">
              <ul class="unordered-list">
                <li><a href="#patients-1">Режим и график работы</a></li>
                <li><a href="#patients-2">Запись на прием</a></li>
                <li><a href="#patients-3">Как подготовиться к приему?</a></li>
                <li><a href="#patients-4">Прием пациентов</a></li>
                <li><a href="#patients-5">Стандарты медицинской помощи</a></li>
                <li><a href="#patients-6">Определения</a></li>
                <li><a href="#patients-7">Оформление документации</a></li>
                <li><a href="#patients-8">Предупреждения</a></li>
                <li><a href="#patients-9">Выдача копий медицинской документации</a></li>
                <li><a href="#patients-10">Как попасть на лечение?</a></li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="patients-1">
        <div class="content">
          <h2>Режим и график работы</h2>
          <div class="icon-block icon-block--clock page-subsection">
            <div class="icon-block__inner">
              <p><strong>Часы работы клиник:</strong></p>
              <p>Ежедневно с 9.00 до 22.00</p>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="patients-2">
        <div class="content">
          <h2>Запись на прием</h2>
          <div class="page-subsection">
            <p>В клинике «Инамед» прием специалистов и оказание медицинских услуг осуществляется по предварительной записи.</p>
            <p>Записаться можно по телефону +7 (495) 204-14-94, а также с помощью <a href="#callback-form">формы записи</a> на сайте или при обращении в регистратуру к медицинскому регистратору.</p>
            <div class="grid grid--justify--center par">
              <a class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12 button par" href="#callback-form">Записаться на консультацию</a>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="patients-3">
        <div class="content">
          <h2>Как подготовиться к приему?</h2>
          <div class="page-subsection">
            <p>Диагностика и лечение проходят максимально результативно, когда пациент становится партнёром врача и активным участником процесса.</p>
            <ol class="ordered-list par">
              <li>Подготовьте список того, о чем хотите сообщить врачу, спросить. Можно выписать всё на бумагу, занести в мобильный телефон.</li>
              <li>Подготовьте список симптомов.<br>Постарайтесь описывать свои ощущения максимально точно, подробно и ничего не скрывать. Также полезно записать, какие болезни вы перенесли ранее, какими сопутствующими патологиями страдаете.</li>
              <li>Подготовьте документацию.<br>Соберите все справки, выписки, результаты анализов и инструментальных исследований, которые у вас есть на руках. То, что в цифровом виде, нужно распечатать. Эта информация сильно поможет врачу.</li>
            </ol>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="patients-4">
        <div class="content">
          <h2>Прием пациентов</h2>
          <div class="page-subsection">
            <p>Прием пациентов осуществляется при наличии документа, удостоверяющего личность (паспорт); при оказании услуг несовершеннолетним (дети до 18 лет) — обязательно сопровождение одним из родителей с предъявлением его паспорта и свидетельства о рождении ребенка.</p>
            <p>Родственники и третьи лица при сопровождении несовершеннолетнего должны иметь нотариально заверенное согласие родителей или законных представителей.</p>
            <h3>Предварительное оповещение пациентов</h3>
            <p>Контакт-центр «Инамед» осуществляет предварительное оповещение пациентов с целью оптимизации записи на прием и в случае изменения в расписании работы специалистов. Клиника не несет ответственности за невозможность предупредить пациента об изменениях в приеме из-за выключенного телефона, из-за неверно указанного телефона или в случае, если пациент не отвечает на звонок.</p>
            <h3>Опоздание на прием</h3>
            <p>Пациенту следует прибыть в клинику за 15 минут до начала приема для надлежащего оформления документов и своевременного начала приема. В случае опоздания Пациента на прием клиника вправе перенести прием на другое время и дату, согласованные с Пациентом, так как опоздание одного ущемляет права другого на своевременный и полноценный прием.</p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="patients-5">
        <div class="content">
          <h2>Стандарты медицинской помощи</h2>
          <div class="page-subsection">
            <p>Врачи клиники «Инамед» в своей работе руководствуются стандартами, утвержденными Минздравом РФ, в соответствии с федеральным законом № 323-ФЗ «Об основах охраны здоровья граждан Российской Федерации» и клиническими рекомендациями по каждой нозологии.</p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="patients-6">
        <div class="content">
          <h2>Определения</h2>
          <div class="page-subsection">
            <h3>Что такое первичный прием?</h3>
            <p>Первичный прием — это первичное обращение пациента к специалисту по поводу заболевания (травмы) или обострения хронического заболевания.</p>
            <h3>Что такое повторный прием?</h3>
            <p>Повторный прием — это повторное обращение пациента к врачу одной и той же специальности по одному и тому же случаю заболевания в течение 1-го месяца с момента первичного обращения.</p>
            <h3>Что такое профилактический прием?</h3>
            <p>Профилактический прием — это комплексный осмотр врача, проводимый с целью раннего (своевременного) выявления состояний, заболеваний и факторов риска их развития, а также в целях определения групп здоровья.</p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="patients-7">
        <div class="content">
          <h2>Оформление документации</h2>
          <div class="page-subsection">
            <p>При первичном обращении с пациентом или его законным представителем заключается договор об оказании платных медицинских услуг, со стороны пациента подписывается информированное добровольное согласие на медицинское вмешательство, заявление о предоставлении информации по электронной почте и оформляется амбулаторная карта. Амбулаторная карта является собственностью клиники и хранится в регистратуре либо в архиве. Для надлежащего оформления документов Пациенту следует прибыть в клинику за 15 минут до начала приема.</p>
            <p>В случаях отказа от подписания договора и добровольного информированного согласия на медицинское вмешательство клиника вправе отказать в приеме.</p>
            <p>Вы можете предварительно ознакомиться с текстом договора на оказание платных медицинских услуг, скачав его с сайта:</p>
            <div class="icon-block icon-block--document icon-block--fit-content par">
              <div class="icon-block__inner"><a href="#">Договор на оказание платных медицинских услуг</a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="patients-8">
        <div class="content">
          <h2>Предупреждения</h2>
          <div class="page-subsection">
            <p>Клиника не участвует в реализации программ государственных гарантий бесплатного оказания гражданам медицинской помощи.</p>
            <p>Клиника вправе отказать в предоставлении услуг и потребовать удалиться лиц, находящихся в состоянии алкогольного, наркотического опьянения, в грязной пачкающей одежде, громко разговаривающих, выражающихся нецензурно или оскорбительно по отношению к посетителям или сотрудникам Клиники.</p>
            <p>При оказании услуг клиника вправе отказать в использовании лекарственных средств и товаров медицинского назначения, предоставленных Пациентом самостоятельно.</p>
            <p>Клиника не несет ответственности за сохранность ценных вещей и денежных средств Пациента, если они не были переданы Клинике по акту.</p>
            <p>Видео- и фото- съемка возможна только после получения разрешения администрации Клиники.</p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="patients-9">
        <div class="content">
          <h2>Выдача копий медицинской документации</h2>
          <div class="page-subsection">
            <p>Выдача копий медицинских документов предоставляется на основании письменного заявления пациента лично заявителю либо его законным представителям или доверенным лицам. Медицинские документы, связанные с функциональной диагностикой, результаты лабораторных исследований и прочее выдаются без дополнительного заявления.</p>
            <p>Законными представителями пациента являются</p>
            <ul class="unordered-list par">
              <li>родители (мать, отец) несовершеннолетнего пациента;</li>
              <li>опекуны несовершеннолетнего пациента или гражданина, лишенного дееспособности в установленном законом порядке.</li>
            </ul>
            <p>Доверенное лицо:</p>
            <ul class="unordered-list par">
              <li>указанное пациентом (законным представителем) как лицо, допущенное к получению сведений, составляющих врачебную тайну, в соответствующем разделе заключенного договора;</li>
              <li>лицо, предъявившее доверенность или согласие от пациента, в отношении которого запрашивается документация, заверенное нотариально либо в установленном законом порядке.</li>
            </ul>
            <p>Срок подготовки копий медицинских документов — до 30 (тридцати) календарных дней.</p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top home-treatment" id="patients-10">
        <div class="content">
          <h2>Как попасть на лечение?</h2>
          <div class="slider page-subsection">
            <div class="slider__slides swiper">
              <div class="slider__wrapper swiper-wrapper">
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Консультация
                    </p>
                    <div class="home-treatment-item__tel">По телефону: <span>+7 (495) 204-14-94</span></div>
                    <div class="home-treatment-item__text">Медицинский консультант расспросит о симптомах, подберет подходящего врача, расскажет про стоимость процедур и запишет на первичный прием.</div>
                    <div class="home-treatment-item__duration">5 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Прием специалиста
                    </p>
                    <div class="home-treatment-item__text">Врач внимательно и спокойно выслушает вас, изучит данные предыдущих исследований, назначит только необходимую диагностику и поставит предварительный диагноз.</div>
                    <div class="home-treatment-item__duration">от 30 до 40 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Диагностика в тот же день
                    </p>
                    <div class="home-treatment-item__text">Врач выполнит осмотр предстательной железы и семявыводящих путей, возьмет биоматериал для анализов. <br> На основе полученных данных врач сможет назначить индивидуальный курс лечения.</div>
                    <div class="home-treatment-item__duration">от 10 до 20 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Повторный прием и лечение
                    </p>
                    <div class="home-treatment-item__text">Врач подробно расскажет о результатах анализа, детально объяснит суть курса лечения, опишет процедуры и препараты. <br> В этот же день вы сможете начать лечение.</div>
                    <div class="home-treatment-item__duration">от 20 до 40 минут</div>
                  </div>
                </div>
              </div>
              <div class="slider__arrow slider__arrow--prev"></div>
              <div class="slider__arrow slider__arrow--next"></div>
              <div class="slider__pagination"></div>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section" id="callback-form">
        <div class="content">
          <h2>Запишитесь на прием сегодня</h2>
          <div class="page-subsection block-marked">
            <p>Администратор свяжется с вами для уточнения даты и времени.</p>
            <form class="grid grid--small-gap--y grid--justify--center grid--align-items--flex-end form par" method="post" action="" data-answer-success='Спасибо за запись! Наш администратор свяжется с вами в ближайшее время для уточнения даты и времени.'>
              <label class="form-input form-input--text grid__cell grid__cell--m--4 grid__cell--s--6 form__input">
                <span class="form-input__label"><strong>Имя *</strong>
                </span>
                <input class="form-input__field" type="text" name="name" required="required" placeholder="Имя" checked="checked"></input>
              </label>
              <label class="form-input form-input--tel grid__cell grid__cell--m--4 grid__cell--s--6 form__input">
                <span class="form-input__label"><strong>Телефон *</strong>
                </span>
                <input class="form-input__field" type="tel" name="tel" required="required" placeholder="+7 (9__) ___-__-__" checked="checked"></input>
              </label>
              <button class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--8 button form__submit" type="submit">Отправить</button>
              <label class="form-input form-input--checkbox grid__cell form__agreement">
                <span class="form-input__label">Отправляя эту форму, вы соглашаетесь с условиями
                  <a class="link" href="#">Обработки персональных данных.
                  </a>
                </span>
                <input class="form-input__field" type="checkbox" required="required" checked="checked"></input>
              </label>
            </form>
          </div>
        </div>
      </section>
    </main>
    <footer class="container page-section page-section--background-color footer">
      <div class="content footer__inner">
        <div class="footer__row footer__row--name">
          <div class="footer__name">Инамед <span>Клиника урологии</span></div>
          <div class="footer__slogan">Лечим иначе!</div>
        </div>
        <!-- <div class="footer__row footer__row--license footer__item footer__item--license">
          a(href='#') Лицензии и сертификаты
        </div> -->
        <div class="footer__row footer__row--copyright">
          <div class="footer__copyright">
            <p class="footer__year">2024, ООО &laquo;Инамед&raquo;</p><a href="#">Правила использования сайтом</a><a href="#">Соглашение на обработку персональных данных</a>
          </div>
          <div class="footer__info">Имеются противопоказания, необходима консультация специалиста.</div>
          <div class="footer__info" style="display: block">Наша клиника располагается на базе сети диагностических центров <a href="https://gorlab.ru/" target="_blank">«Горлаб»</a></div>
        </div>
      </div>
    </footer>
  </body>
  <link rel="stylesheet" href="/local/front/template/styles/style.rest.css?v<?php require __DIR__ . '/version.php'?>"/>
  <link rel="stylesheet" href="/local/front/template/styles/methods.rest.css?v<?php require __DIR__ . '/version.php'?>"/>
  <script src="/local/front/template/scripts/script.js?v<?php require __DIR__ . '/version.php'?>"></script>
  <script src="/local/front/template/scripts/methods.js?v<?php require __DIR__ . '/version.php'?>"></script>
</html>
