<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Информация о клинике &#34;Инамед&#34;: современные методы лечения простатита, синдрома хронической тазовой боли, высококвалифицированные специалисты, комплексная диагностика. Запись на прием по телефону">
    <link rel="stylesheet" href="/local/front/template/styles/style.core.css?v=<?php require __DIR__ . '/version.php'?>"/>
    <link rel="stylesheet" href="/local/front/template/styles/about.core.css?v=<?php require __DIR__ . '/version.php'?>"/>
      <title>О клинике "Инамед"</title>
      <!-- Метрика -->
      <?php require __DIR__ . '/includes/metrika.php' ?>
  </head>
  <body>
    <header class="header">
      <div class="container header__inner">
        <div class="content">
          <a class="logo header__logo" href="/"></a>
            <div class="header__name"> <a href="/">Инамед </a><span>Клиника урологии</span></div><a class="header__call" href="tel:+74952041494" aria-label="Позвонить"></a>
            <button class="header__switcher js-header-switcher hidden-l" type="button" aria-label="Открыть или закрыть мобильное меню"></button>
            <?php require __DIR__ . '/includes/menu.php'?>

        </div>
      </div>
      <div class="container header__bottom">
        <div class="content">
          <p class="text-marked text-marked--size--l text-align-center">Излечиваем хронический простатит (СХТБ) с <strong>эффективностью 87%</strong>.
          </p>
        </div>
      </div>
    </header>
    <main>
      <div class="container breadcrumbs">
        <div class="content breadcrumbs__inner"><a class="breadcrumbs__parent" href="/">Главная</a>
          <div class="breadcrumbs__current">О клинике</div>
        </div>
      </div>
      <section class="container page-section">
        <div class="content">
          <h1>О клинике</h1>
          <div class="accordion-item page-subsection">
            <div class="accordion-item__header">Оглавление
              <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                  <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                  <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                </path>
              </svg>
            </div>
            <div class="accordion-item__body">
              <ul class="unordered-list">
                <li><a href="#about-1">История</a></li>
                <li><a href="#about-2">Миссия, принципы и ценности</a></li>
                <li><a href="#about-3">Специализация</a></li>
                <li><a href="#about-4">Наши преимущества</a></li>
                <li><a href="#about-5">Что мы лечим?</a></li>
                <li><a href="#about-6">Как выбрать доктора?</a></li>
                <li><a href="#about-7">Фото клиники</a></li>
                <li><a href="#about-8">Как попасть на лечение?</a></li>
              </ul>
            </div>
          </div>
          <div class="page-subsection">
            <div class="block-marked block-marked--bordered">
              <span class="text-marked text-marked--size--xl text-marked--pag--p">Наша клиника основана пациентом во благо пациентов.
              </span>
            </div>
          </div>
          <div class="page-subsection" id="about-1">
            <h2>История</h2>
            <p>Меня зовут Виктор, я&nbsp;один из&nbsp;основателей клиники &laquo;Инамед&raquo;. В&nbsp;прошлом <strong>я&nbsp;пациент</strong> с&nbsp;синдромом хронической тазовой боли (хронический простатит).</p>
            <p>Я&nbsp;прошел через многие <strong>испытания</strong>.</p>
            <p>Безуспешно лечился около <strong>2-х лет</strong>, был у&nbsp;7&nbsp;урологов в&nbsp;двух крупнейших городах России.</p>
            <p>Каждый раз это превращалось в&nbsp;<strong>стандартное лечение</strong>, с&nbsp;большим количеством различных исследований. А&nbsp;иногда&nbsp;&mdash; рекомендации обратиться к&nbsp;психотерапевту.</p>
            <p>Прошел разные методы лечения: массаж предстательной железы, антибиотики, альфа адреноблокаторы, антидепрессанты, нестероидные противовоспалительные препараты, различные свечи, венотоники, народные средства, целители, лазеротерапия и&nbsp;т.д.</p>
            <p>При общении с&nbsp;врачами сталкивался с&nbsp;непониманием, <strong>безразличием</strong>, хамством и <strong>&nbsp;&laquo;выкачиванием&raquo; денег</strong>. Что еще больше подрывало мою веру в&nbsp;излечение.</p>
            <p>Но&nbsp;в&nbsp;итоге нашел методику, которая отражает <strong>иной взгляд</strong> на&nbsp;болезнь и&nbsp;<strong>смог вылечиться</strong>.</p>
          </div>
          <div class="block-marked page-subsection">
            <p>Идея создания данной клиники возникла после того, как я&nbsp;понял, что не&nbsp;хочу, чтобы кто-либо в&nbsp;мире остался <strong>один на&nbsp;один</strong> с&nbsp;этим заболеванием.</p>
          </div>
          <div class="page-subsection">
            <p>Я&nbsp;нашел партнеров с&nbsp;<strong>опытом и&nbsp;компетенциями</strong> в&nbsp;управлении медицинской организацией, которые воодушевились моей идеей.</p>
            <p>Мы&nbsp;решили объединить <strong>наши знания, опыт</strong> и&nbsp;создать урологическую клинику, где:</p>
            <ul class="unordered-list par">
              <li>сконцентрированы компетенции в&nbsp;одной сфере</li>
              <li>используется эффективная методика лечения</li>
              <li>хорошо понимают пациентов с&nbsp;данным заболеванием</li>
            </ul>
            <p>За&nbsp;время лечения я&nbsp;получил <strong>огромный объем знаний</strong> о&nbsp;данном заболевании, а&nbsp;также пообщался со&nbsp;многими пациентами. Это помогло мне понять, во-первых, как сделать <strong>лечение комплексным и&nbsp;эффективным</strong>. А, во-вторых, как создать лучший клиентский сервис с&nbsp;<strong>заботой о&nbsp;пациентах</strong>.</p>
            <p>Мы&nbsp;нашли высококлассных специалистов, которые прошли длительное <strong>обучение и&nbsp;стажировку</strong> у&nbsp;автора методики, который занимается этой проблематикой около 30&nbsp;лет.</p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--background-color">
      <div class="content">
          <h2>Остались вопросы?</h2>
          <div class="page-subsection par">
            <p>Свяжитесь с нами и <strong>узнайте подробности</strong> о заболевании, методике лечения, ценах.</p>
          </div>
          <div class="grid grid--justify--center par">
            <a class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--8 button par" href="tel:+74952041494">Позвонить</a>
          </div>
        </div>
      </section>
      <section class="container page-section" id="about-2">
        <div class="content">
          <h2>Миссия, принципы и ценности</h2>
          <div class="grid page-subsection">
            <div class="block-marked grid__cell grid__cell--m--6">
              <h3>Миссия</h3>
              <p>Наша миссия – организовывать, доступное в различных регионах, эффективное, комплексное, учитывающее интересы и потребности лечение и реабилитацию пациентов с хроническим простатитом (синдромом хронической тазовой боли), которое возвращает их к полноценной жизни.</p>
            </div>
            <div class="block-marked grid__cell grid__cell--m--6">
              <h3>Ответственность</h3>
              <p>Мы создали клинику, где сможем <strong>контролировать все процессы</strong> и быть уверены, что каждому пациенту, на всех этапах лечения, будет оказана необходимая медицинская и диагностическая помощь на <strong>должном уровне</strong>.</p>
            </div>
            <div class="block-marked grid__cell grid__cell--m--6">
              <h3>Отношение</h3>
              <p>Лучше других понимаем пациентов с синдромом хронической тазовой боли. Уделяем пациенту достаточно времени, относимся к вниманием и пониманием, искренне желаем помочь. Учитываем желания и потребности пациента.</p>
            </div>
            <div class="block-marked grid__cell grid__cell--m--6">
              <h3>Индивидуальный подход</h3>
              <p>Двух одинаковых больных нет, даже, если у них один диагноз. Различия могут быть как физиологическими, так и психологическими. Поэтому мы учитываем это при выборе тактики терапии и <strong>не лечим по шаблонам</strong>.</p>
              <p>Варианты диагностики, необходимые лечебные и реабилитационные методики подбираем <strong>индивидуально</strong>.</p>
            </div>
            <div class="block-marked grid__cell grid__cell--m--12">
              <h3>Честность</h3>
              <p>Не&nbsp;&laquo;продаем&raquo; ненужные медицинские услуги и&nbsp;<strong>не&nbsp;назначаем лишние анализы</strong> и&nbsp;диагностику.</p>
              <p>Полы в нашей клинике не выложены мрамором, который заложен в стоимость услуг.</p>
              <p>Стараемся придерживаться <strong>адекватной</strong> ценовой политики.</p>
              <p>В стоимость лечения включены все услуги, <strong>отсутствуют скрытые платежи и переплаты</strong>.</p>
            </div>
          </div>
          <div class="grid grid--justify--center page-subsection">
            <a class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12 button" href="#callback-form">Записаться на консультацию</a>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--background-color" id="about-3">
        <div class="content">
          <h2>Специализация</h2>
          <div class="block-marked block-marked--white page-subsection">
            <p>Клиника &laquo;Инамед&raquo;&nbsp;&mdash; <strong>центр компетенций</strong> в&nbsp;лечении хронического простатита (синдрома хронической тазовой боли).</p>
          </div>
          <div class="page-subsection">
            <p>Мы&nbsp;<strong>специализированная клиника</strong>&nbsp;&mdash; сконцентрировали компетенции в&nbsp;одной сфере, в&nbsp;которой хорошо разбираемся. Глубокое <strong>понимание причин заболевания</strong>, состояния пациента и&nbsp;подхода к&nbsp;лечению.</p>
            <p>Используем <strong>накопленный за&nbsp;30-лет опыт</strong> в&nbsp;лечении хронического простатита (синдрома хронической тазовой боли).</p>
          </div>
        </div>
      </section>
      <section class="container page-section" id="about-4">
        <div class="content">
          <h2>Наши преимущества</h2>
          <div class="grid page-subsection">
            <div class="block-marked block-marked--bordered grid__cell grid__cell--l--4 grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue"><strong>Революционная методика</strong>
              </p>
              <p><strong>Отличная от других</strong> методика. Единственная, которая направлена на <strong>истинную причину болезни</strong>, а не на симптомы.</p>
            </div>
            <div class="block-marked block-marked--bordered grid__cell grid__cell--l--4 grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue"><strong>Специализация</strong>
              </p>
              <p>Сконцентрировали компетенции в одной сфере. Глубоко понимаем <strong>причины заболевания</strong>.</p>
            </div>
            <div class="block-marked block-marked--bordered grid__cell grid__cell--l--4 grid__cell--m--6">
              <p class="text-marked text-marked--size--xl text-marked--color--blue"><strong>Внимание к вашей проблеме</strong>
              </p>
              <p>Лучше других <strong>понимаем пациентов</strong> с хроническим простатитом.</p>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="about-5">
        <div class="content">
          <h2>Что мы лечим?</h2>
          <p>Используем единственную методику, которая направлена не на симптомы, на истинную причину болезней:</p>
          <ul class="unordered-list unordered-list--marked unordered-list--no-margin grid grid--small-gap--y par">
            <li class="grid__cell grid__cell--s--6 grid__cell--xs--12">Простатит
            </li>
            <li class="grid__cell grid__cell--s--6 grid__cell--xs--12">Заболевания простаты
            </li>
            <li class="grid__cell grid__cell--s--6 grid__cell--xs--12">Синдром хронической тазовой боли
            </li>
          </ul>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="about-6">
        <div class="content">
          <h2>Как выбрать доктора?</h2>
          <div class="grid page-subsection">
            <div class="grid__cell grid__cell--m--6 grid__cell--xs--12">
              <p>Результат лечения напрямую зависит от:</p>
              <ul class="unordered-list unordered-list--marked grid grid--small-gap--y par">
                <li class="grid__cell grid__cell--xs--12">Эффективности доктора: опыт других пациентов, отраженный &laquo;кейсах&raquo; и&nbsp;отзывах</li>
                <li class="grid__cell grid__cell--xs--12">Специализация клиники</li>
                <li class="grid__cell grid__cell--xs--12">Знаний, опыта и&nbsp;компетенций доктора в&nbsp;конкретной области</li>
              </ul>
            </div>
            <div class="grid__cell grid__cell--m--6 grid__cell--xs--12">
              <h3>Как сделать правильный выбор?</h3>
              <ol class="ordered-list ordered-list--marked grid grid--small-gap--y page-subsection">
                <li class="grid__cell grid__cell--xs--12">Выбирайте не клинику, а доктора
                </li>
                <li class="grid__cell grid__cell--xs--12">Принимайте во внимание специализацию врача и клиники
                </li>
                <li class="grid__cell grid__cell--xs--12">Оценивайте реальные результаты работы клиники
                </li>
              </ol>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="about-7">
        <div class="content">
          <h2>Фото клиники</h2>
          <div class="slider home-gallery page-subsection">
            <div class="slider__slides swiper">
              <div class="slider__wrapper swiper-wrapper">
                <div class="slide swiper-slide">
                  <picture>
                    <source srcset="/local/front/images/photo-1.webp, /local/front/images/photo-1@2x.webp 2x"/>
                    <source srcset="/local/front/images/photo-1.jpg, /local/front/images/photo-1@2x.jpg 2x"/><img class="image-border-radius-s" src="/local/front/images/photo-1.jpg" loading="lazy" decoding="async"/>
                  </picture>
                </div>
                <div class="slide swiper-slide">
                  <picture>
                    <source srcset="/local/front/images/photo-2.webp, /local/front/images/photo-2@2x.webp 2x"/>
                    <source srcset="/local/front/images/photo-2.jpg, /local/front/images/photo-2@2x.jpg 2x"/><img class="image-border-radius-s" src="/local/front/images/photo-2.jpg" loading="lazy" decoding="async"/>
                  </picture>
                </div>
                <div class="slide swiper-slide">
                  <picture>
                    <source srcset="/local/front/images/photo-3.webp, /local/front/images/photo-3@2x.webp 2x"/>
                    <source srcset="/local/front/images/photo-3.jpg, /local/front/images/photo-3@2x.jpg 2x"/><img class="image-border-radius-s" src="/local/front/images/photo-3.jpg" loading="lazy" decoding="async"/>
                  </picture>
                </div>
              </div>
              <div class="slider__arrow slider__arrow--prev"></div>
              <div class="slider__arrow slider__arrow--next"></div>
              <div class="slider__pagination"></div>
            </div>
          </div>
        </div>
      </section>
      <!--+pageSection({ tag: 'section', noPadding:'top' })-->
      <!--  h2 Лицензии-->
      <!--  +slider.licenses-gallery.page-subsection-->
      <!--    - for (let i = 1; i < 5; i++)-->
      <!--      +slide-->
      <!--        a(href='//local/front/images/photo.jpg' data-glightbox data-gallery='licenses')-->
      <!--          img(src="/local/front/images/photo.jpg", alt="Лицензии").image-border-radius-s-->
      <section class="container page-section page-section--no-padding--top home-treatment" id="about-8">
        <div class="content">
          <h2>Как попасть на лечение?</h2>
          <div class="slider page-subsection">
            <div class="slider__slides swiper">
              <div class="slider__wrapper swiper-wrapper">
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Консультация
                    </p>
                    <div class="home-treatment-item__tel">По телефону: <span>+7 (495) 204-14-94</span></div>
                    <div class="home-treatment-item__text">Медицинский консультант расспросит о симптомах, подберет подходящего врача, расскажет про стоимость процедур и запишет на первичный прием.</div>
                    <div class="home-treatment-item__duration">5 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Прием специалиста
                    </p>
                    <div class="home-treatment-item__text">Врач внимательно и спокойно выслушает вас, изучит данные предыдущих исследований, назначит только необходимую диагностику и поставит предварительный диагноз.</div>
                    <div class="home-treatment-item__duration">от 30 до 40 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Диагностика в тот же день
                    </p>
                    <div class="home-treatment-item__text">Врач выполнит осмотр предстательной железы и семявыводящих путей, возьмет биоматериал для анализов. <br> На основе полученных данных врач сможет назначить индивидуальный курс лечения.</div>
                    <div class="home-treatment-item__duration">от 10 до 20 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Повторный прием и лечение
                    </p>
                    <div class="home-treatment-item__text">Врач подробно расскажет о результатах анализа, детально объяснит суть курса лечения, опишет процедуры и препараты. <br> В этот же день вы сможете начать лечение.</div>
                    <div class="home-treatment-item__duration">от 20 до 40 минут</div>
                  </div>
                </div>
              </div>
              <div class="slider__arrow slider__arrow--prev"></div>
              <div class="slider__arrow slider__arrow--next"></div>
              <div class="slider__pagination"></div>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="callback-form">
        <div class="content">
          <h2>Запишитесь на прием сегодня</h2>
          <div class="page-subsection block-marked">
            <p>Администратор свяжется с вами для уточнения даты и времени.</p>
            <form class="grid grid--small-gap--y grid--justify--center grid--align-items--flex-end form par" method="post" action="" data-answer-success='Спасибо за запись! Наш администратор свяжется с вами в ближайшее время для уточнения даты и времени.'>
              <label class="form-input form-input--text grid__cell grid__cell--m--4 grid__cell--s--6 form__input">
                <span class="form-input__label"><strong>Имя *</strong>
                </span>
                <input class="form-input__field" type="text" name="name" required="required" placeholder="Имя" checked="checked"></input>
              </label>
              <label class="form-input form-input--tel grid__cell grid__cell--m--4 grid__cell--s--6 form__input">
                <span class="form-input__label"><strong>Телефон *</strong>
                </span>
                <input class="form-input__field" type="tel" name="tel" required="required" placeholder="+7 (9__) ___-__-__" checked="checked"></input>
              </label>
              <button class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--8 button form__submit" type="submit">Отправить</button>
              <label class="form-input form-input--checkbox grid__cell form__agreement">
                <span class="form-input__label">Отправляя эту форму, вы соглашаетесь с условиями
                  <a class="link" href="#">Обработки персональных данных.
                  </a>
                </span>
                <input class="form-input__field" type="checkbox" required="required" checked="checked"></input>
              </label>
            </form>
          </div>
        </div>
      </section>
    </main>
    <footer class="container page-section page-section--background-color footer">
      <div class="content footer__inner">
        <div class="footer__row footer__row--name">
          <div class="footer__name">Инамед <span>Клиника урологии</span></div>
          <div class="footer__slogan">Лечим иначе!</div>
        </div>
        <!-- <div class="footer__row footer__row--license footer__item footer__item--license">
          a(href='#') Лицензии и сертификаты
        </div> -->
        <div class="footer__row footer__row--copyright">
          <div class="footer__copyright">
            <p class="footer__year">2024, ООО &laquo;Инамед&raquo;</p><a href="#">Правила использования сайтом</a><a href="#">Соглашение на обработку персональных данных</a>
          </div>
          <div class="footer__info">Имеются противопоказания, необходима консультация специалиста.</div>
          <div class="footer__info" style="display: block">Наша клиника располагается на базе сети диагностических центров <a href="https://gorlab.ru/" target="_blank">«Горлаб»</a></div>
        </div>
      </div>
    </footer>
  </body>
  <link rel="stylesheet" href="/local/front/template/styles/style.rest.css?v=<?php require __DIR__ . '/version.php'?>"/>
  <link rel="stylesheet" href="/local/front/template/styles/about.rest.css?v=<?php require __DIR__ . '/version.php'?>"/>
  <script src="/local/front/template/scripts/script.js?v=<?php require __DIR__ . '/version.php'?>"></script>
  <script src="/local/front/template/scripts/about.js?v=<?php require __DIR__ . '/version.php'?>"></script>
</html>
