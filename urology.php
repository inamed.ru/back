<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Какие бывают урологические болезни, отвечают врачи урологической клиники &#34;Инамед&#34;: современные методы лечения простатита, синдрома хронической тазовой боли, высококвалифицированные специалисты, комплексная диагностика. Запись на прием по телефону">
    <link rel="stylesheet" href="/local/front/template/styles/style.core.css?v=<?php require __DIR__ . '/version.php'?>"/>
    <link rel="stylesheet" href="/local/front/template/styles/methods.core.css?v=<?php require __DIR__ . '/version.php'?>"/>
    <title>Урологические заболевания: причины, симптомы, диагностика и лечение</title>
      <!-- Метрика -->
      <?php require __DIR__ . '/includes/metrika.php' ?>
  </head>
  <body>
    <header class="header">
      <div class="container header__inner">
        <div class="content">
          <a class="logo header__logo" href="/"></a>
            <div class="header__name"> <a href="/">Инамед </a><span>Клиника урологии</span></div><a class="header__call" href="tel:+74952041494" aria-label="Позвонить"></a>
            <button class="header__switcher js-header-switcher hidden-l" type="button" aria-label="Открыть или закрыть мобильное меню"></button>
            <?php require __DIR__ . '/includes/menu.php'?>
        </div>
      </div>
      <div class="container header__bottom">
        <div class="content">
          <p class="text-marked text-marked--size--l text-align-center">Излечиваем хронический простатит (СХТБ) с <strong>эффективностью 87%</strong>.
          </p>
        </div>
      </div>
    </header>
    <main>
      <div class="container breadcrumbs">
        <div class="content breadcrumbs__inner"><a class="breadcrumbs__parent" href="/">Главная</a>
          <div class="breadcrumbs__current">Мы лечим</div>
        </div>
      </div>
      <section class="container page-section">
        <div class="content">
          <h1>Урология</h1>
          <div class="accordion-item page-subsection">
            <div class="accordion-item__header">Оглавление
              <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                  <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                  <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                </path>
              </svg>
            </div>
            <div class="accordion-item__body">
              <ul class="unordered-list">
                <li><a href="#urology-1">В каких случаях обращаться к урологу?</a></li>
                <li><a href="#urology-2">Мы лечим</a></li>
                <li><a href="#urology-3">Диагностика</a></li>
                <li><a href="#urology-4">Методы лечения</a></li>
              </ul>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--background-color">
        <div class="content">
          <div class="grid grid--align-items--center grid--justify--space-between">
            <ul class="unordered-list unordered-list--marked grid__cell grid__cell--m--7 grid__cell--s--5 grid__cell--xs--12">
              <li>Революционная методика лечения</li>
              <li>Специализация в данной сфере</li>
              <li>Внимание к вашей проблеме</li>
            </ul><a class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12 button" href="tel:+74952041494">Записаться на консультацию</a>
          </div>
        </div>
      </section>
      <section class="container page-section">
        <div class="content">
          <div class="page-subsection">
            <picture>
              <source srcset="/local/front/images/urology-1.webp"/>
              <source srcset="/local/front/images/urology-1.jpg"/><img src="/local/front/images/urology-1.jpg" loading="lazy" decoding="async"/>
            </picture>
          </div>
          <div class="page-subsection">
            <p>Урология как область медицинской науки занимается изучением мочевыводящей и связанной с ней половой системы человека, происходящих в них физиологических и патологических процессов, диагностикой, профилактикой и лечением заболеваний мочеполовых органов (уретры, мочевого пузыря, мочеточников, почек, мужских половых органов).</p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="urology-1">
        <div class="content">
          <h2>В каких случаях обращаться к урологу?</h2>
          <div class="page-subsection">
            <p>Если вас беспокоят один или несколько таких симптомов, вам <strong>необходима консультация</strong> нашего уролога:</p>
          </div>
          <ul class="unordered-list unordered-list--marked grid grid--small-gap--y page-subsection">
            <li class="grid__cell grid__cell--m--6 grid__cell--xs--12">Боль в паховой области, которая может отдавать в яичко, половой член, внутреннюю поверхность бедра, копчик, задний проход
            </li>
            <li class="grid__cell grid__cell--m--6 grid__cell--xs--12">Затрудненное или учащенное мочеиспускание, неудержимые позывы
            </li>
            <li class="grid__cell grid__cell--m--6 grid__cell--xs--12">Снижение эрекции
            </li>
            <li class="grid__cell grid__cell--m--6 grid__cell--xs--12">Боли усиливаются после эякуляции или мочеиспускания
            </li>
            <li class="grid__cell grid__cell--m--6 grid__cell--xs--12">Наличие желтой слизи в эякуляте
            </li>
          </ul>
          <div class="page-subsection">
            <picture>
              <source srcset="/local/front/images/urology-2.webp"/>
              <source srcset="/local/front/images/urology-2.jpg"/><img src="/local/front/images/urology-2.jpg" loading="lazy" decoding="async"/>
            </picture>
          </div>
          <div class="icon-block icon-block--warning page-subsection">
            <div class="icon-block__inner">
              <p>В случае возникновения первых симптомов дискомфорта <strong>не</strong> нужно <strong>заниматься самолечением.</strong></p>
              <p>Это может вызвать <strong>серьезные последствия</strong> и усугубить течение болезни.</p>
              <p>Обращайтесь только к проверенным и грамотным специалистам.</p>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="urology-2">
        <div class="content">
          <h2>Мы лечим</h2>
          <div class="page-subsection">
            <p>Используем единственную методику, которая направлена не на симптомы, на истинную причину болезней:</p>
            <ul class="unordered-list par">
              <li>Простатит</li>
              <li>Заболевания простаты</li>
              <li>Синдром хронической тазовой боли</li>
            </ul>
          </div>
          <div class="block-marked page-subsection">
            <p>На небактериальный хронический простатит приходится <strong>90% случаев</strong> всех диагнозов простатита.</p>
          </div>
          <div class="page-subsection">
            <p>В течении очень длительного времени, <strong>около века</strong>, любые жалобы пациента мужского пола на болевой синдром в области малого таза, в конечном счете, замыкаются на диагнозе — <strong>хронический простатит</strong>.</p>
            <p>В действительности простатит — довольно <strong>запутанная научная и практическая проблема</strong>. Специалистов, которые разбираются в ней глубоко и всесторонне очень мало. Существует множество мифов вокруг этой болезни.</p>
            <p><strong>Синдром хронической тазовой боли</strong> (СХТБ) — это проявление хронической тазовой боли, когда отсутствуют доказанная инфекция или другие местные патологические изменения (без очевидных причин и механизмов заболевания), объясняющие наличие боли.</p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="urology-3">
        <div class="content">
          <h2>Диагностика</h2>
          <div class="page-subsection">
            <p>Результаты обследований пациент получит в сжатые сроки и с доступной для понимания расшифровкой.</p>
          </div>
          <ul class="unordered-list unordered-list--marked grid grid--small-gap--y page-subsection">
            <li class="grid__cell grid__cell--xs--12"> Исследования
            </li>
            <li class="grid__cell grid__cell--xs--12"> Анализы
            </li>
          </ul>
          <div class="page-subsection">
            <p>Диагностика в урологии — важный этап для определения диагноза и назначения верного лечения при лечении хронического небактериального простатита и СХТБ.</p>
            <p>Диагностика предполагает <strong>исключение других потенциальных причин симптомов</strong>, таких как бактериальный простатит, доброкачественная гипертрофия предстательной железы, гиперактивный мочевой пузырь, опухоли мочеполовой системы, стриктура уретры, нейрогенные расстройства мочевого пузыря и другие специфические заболевания органов мочеполовой системы, вызывающие тазовую боль.</p>
          </div>
          <div class="page-subsection">
            <picture>
              <source srcset="/local/front/images/urology-3_s.webp, /local/front/images/urology-3_s@2x.webp 2x" media="(max-width: 479px)"/>
              <source srcset="/local/front/images/urology-3_l.webp, /local/front/images/urology-3_l@2x.webp 2x" media="(min-width: 480px) and (max-width: 999px)"/>
              <source srcset="/local/front/images/urology-3_xl.webp, /local/front/images/urology-3_xl@2x.webp 2x" media="(min-width: 1000px)"/>
              <source srcset="/local/front/images/urology-3_s.jpg, /local/front/images/urology-3_s@2x.jpg 2x" media="(max-width: 479px)"/>
              <source srcset="/local/front/images/urology-3_l.jpg, /local/front/images/urology-3_l@2x.jpg 2x" media="(min-width: 480px) and (max-width: 999px)"/>
              <source srcset="/local/front/images/urology-3_xl.jpg, /local/front/images/urology-3_xl@2x.jpg 2x" media="(min-width: 1000px)"/><img src="/local/front/images/urology-3.jpg" loading="lazy" decoding="async"/>
            </picture>
          </div>
          <div class="page-subsection">
            <p>Первым важным шагом в оценке хронического небактериального простатита или СХТБ является тщательный <strong>сбор анамнеза</strong> (сведений о возникновении и течении болезни). Он включает тип боли и ее локализацию, распространение, характер ощущений, а также симптомы нижних мочевых путей, сексуальной дисфункции и других сопутствующих поражений.</p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="urology-4">
        <div class="content">
          <h2>Методы лечения</h2>
          <div class="page-subsection">
            <p>Чтобы лечение было максимально эффективным, каждому пациенту мы предлагаем индивидуальный комплекс процедур.</p>
          </div>
          <ul class="unordered-list unordered-list--marked grid grid--small-gap--y page-subsection">
            <li class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12">Физиотерапия
            </li>
            <li class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12">Пальцевой массаж
            </li>
            <li class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12">Вибрационный массаж
            </li>
            <li class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12">Ударно волновая терапия
            </li>
            <li class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12">Магнитотерапия
            </li>
            <li class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12">Термотерапия
            </li>
            <li class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12">Электрофорез
            </li>
            <li class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12">Электростимуляция
            </li>
            <li class="grid__cell grid__cell--m--4 grid__cell--s--6 grid__cell--xs--12">Аккупунктура
            </li>
          </ul>
          <div class="page-subsection">
            <p>При лечении хронического небактериального простатита или СХТБ мы используем в основном <strong>физиотерапевтические методы лечения</strong>, направленные на исчезновение патологии, которая является причиной возникновения боли и других симптомов.</p>
            <p>В конце курса лечения иногда <strong>остаются слабыe мигрирующие боли в разных зонах</strong>, связанных через нервы.</p>
            <p>Дальнейшее лечение проходит с применением длинных <strong>акупунктурных игл.</strong></p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top home-treatment" id="methods-4">
        <div class="content">
          <h2>Как попасть на лечение?</h2>
          <div class="slider page-subsection">
            <div class="slider__slides swiper">
              <div class="slider__wrapper swiper-wrapper">
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Консультация
                    </p>
                    <div class="home-treatment-item__tel">По телефону: <span>+7 (495) 204-14-94</span></div>
                    <div class="home-treatment-item__text">Медицинский консультант расспросит о симптомах, подберет подходящего врача, расскажет про стоимость процедур и запишет на первичный прием.</div>
                    <div class="home-treatment-item__duration">5 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Прием специалиста
                    </p>
                    <div class="home-treatment-item__text">Врач внимательно и спокойно выслушает вас, изучит данные предыдущих исследований, назначит только необходимую диагностику и поставит предварительный диагноз.</div>
                    <div class="home-treatment-item__duration">от 30 до 40 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Диагностика в тот же день
                    </p>
                    <div class="home-treatment-item__text">Врач выполнит осмотр предстательной железы и семявыводящих путей, возьмет биоматериал для анализов. <br> На основе полученных данных врач сможет назначить индивидуальный курс лечения.</div>
                    <div class="home-treatment-item__duration">от 10 до 20 минут</div>
                  </div>
                </div>
                <div class="slide swiper-slide home-treatment-item">
                  <div class="home-treatment-item__inner">
                    <p class="text-marked text-marked--size--xl text-marked--color--blue home-treatment-item__title">Повторный прием и лечение
                    </p>
                    <div class="home-treatment-item__text">Врач подробно расскажет о результатах анализа, детально объяснит суть курса лечения, опишет процедуры и препараты. <br> В этот же день вы сможете начать лечение.</div>
                    <div class="home-treatment-item__duration">от 20 до 40 минут</div>
                  </div>
                </div>
              </div>
              <div class="slider__arrow slider__arrow--prev"></div>
              <div class="slider__arrow slider__arrow--next"></div>
              <div class="slider__pagination"></div>
            </div>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="methods-5">
        <div class="content">
          <h2>Цены</h2>
          <div class="page-subsection">
            <div class="table-wrap">
              <table class="table">
                <colgroup class="table__colgroup table__colgroup--2">
                  <col class="table__col"/>
                  <col class="table__col"/>
                </colgroup>
                <tr class="table__row">
                  <td class="table__cell">Прием (осмотр консультация) врача-уролога первичный
                  </td>
                  <td class="table__cell text-align-center">3500 руб.
                  </td>
                </tr>
                <tr class="table__row">
                  <td class="table__cell">Прием (осмотр консультация) врача-уролога вторичный
                  </td>
                  <td class="table__cell text-align-center">2500 руб.
                  </td>
                </tr>
              </table>
            </div>
            <p class="text-marked text-marked--size--s text-marked--color--grey">Указанные на сайте цены не являются публичной офертой. Стоимость услуг зависит от уровня квалификации специалиста.
            </p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top" id="methods-6">
        <div class="content">
          <h2>Частые вопросы</h2>
          <div class="page-subsection grid">
            <div class="grid__cell grid__cell--m--9 grid__cell--xs--12">
              <div class="accordion-item par">
                <div class="accordion-item__header">Как долго длится лечение?
                  <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                      <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                      <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                    </path>
                  </svg>
                </div>
                <div class="accordion-item__body">
                  <p>Длительность лечения зависит от&nbsp;времени, которое вы&nbsp;болеете, а&nbsp;также от&nbsp;реакции организма на&nbsp;процесс лечения.</p>
                </div>
              </div>
              <div class="accordion-item par">
                <div class="accordion-item__header">Какие методы вы&nbsp;используете для лечения?
                  <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                      <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                      <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                    </path>
                  </svg>
                </div>
                <div class="accordion-item__body">
                  <p>Мы&nbsp;используем в&nbsp;основном физиотерапевтические методы лечения. Но&nbsp;область воздействия, а&nbsp;также способ принципиально отличается от&nbsp;обычных методов лечения хронического простатита.</p>
                </div>
              </div>
              <div class="accordion-item par">
                <div class="accordion-item__header">Почему я&nbsp;раньше не&nbsp;слышал о&nbsp;данной методике?
                  <svg class="accordion-item__arrow" width="27" height="16" viewBox="0 0 584 345" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" stroke="black" stroke-width="72" stroke-linecap="round">
                      <animate id="open" begin="indefinite" dur=".1s" attributeName="d" from="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" to="M 36 306.999 L 290.861 51.821 L 547.997 308.653" fill="freeze"></animate>
                      <animate id="close" begin="indefinite" dur=".4s" attributeName="d" from="M 36 306.999 L 290.861 51.821 L 547.997 308.653" to="M 36.002 36.5 L 289.208 293.319 L 547.999 38.154" fill="freeze"></animate>
                    </path>
                  </svg>
                </div>
                <div class="accordion-item__body">
                  <p>Долгое время данная методика применялась только в&nbsp;одной клинике в&nbsp;стране и&nbsp;никак не&nbsp;рекламировалась. Информация передавалась по&nbsp;&laquo;сарафанному радио&raquo; от&nbsp;пациента к&nbsp;пациенту.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <footer class="container page-section page-section--background-color footer">
      <div class="content footer__inner">
        <div class="footer__row footer__row--name">
          <div class="footer__name">Инамед <span>Клиника урологии</span></div>
          <div class="footer__slogan">Лечим иначе!</div>
        </div>
        <!-- <div class="footer__row footer__row--license footer__item footer__item--license">
          a(href='#') Лицензии и сертификаты
        </div> -->
        <div class="footer__row footer__row--copyright">
          <div class="footer__copyright">
            <p class="footer__year">2024, ООО &laquo;Инамед&raquo;</p><a href="#">Правила использования сайтом</a><a href="#">Соглашение на обработку персональных данных</a>
          </div>
          <div class="footer__info">Имеются противопоказания, необходима консультация специалиста.</div>
          <div class="footer__info" style="display: block">Наша клиника располагается на базе сети диагностических центров <a href="https://gorlab.ru/" target="_blank">«Горлаб»</a></div>
        </div>
      </div>
    </footer>
  </body>
  <link rel="stylesheet" href="/local/front/template/styles/style.rest.css?v=<?php require __DIR__ . '/version.php'?>"/>
  <link rel="stylesheet" href="/local/front/template/styles/methods.rest.css?v=<?php require __DIR__ . '/version.php'?>"/>
  <script src="/local/front/template/scripts/script.js?v=<?php require __DIR__ . '/version.php'?>"></script>
  <script src="/local/front/template/scripts/methods.js?v=<?php require __DIR__ . '/version.php'?>"></script>
</html>
