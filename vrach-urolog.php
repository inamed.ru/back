<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Клиника лечение простатита в Москве - урология "Инамед"</title>
    <meta name="description" content="Клиника лечения простатита в Москве &#34;Инамед&#34;, современные методы лечения синдрома хронической тазовой боли, высококвалифицированные специалисты, комплексная диагностика. Запись на прием по телефону">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="/local/front/template/styles/style.core.css?v=<?php require __DIR__ . '/version.php'?>"/>
    <link rel="stylesheet" href="/local/front/template/styles/vacancy.core.css?v=<?php require __DIR__ . '/version.php'?>"/>
      <?php require __DIR__ . '/includes/metrika.php' ?>
      <?php require __DIR__ . '/code.php'?>
  </head>
  <body>
    <header class="header">
      <div class="container header__inner">
        <div class="content">
          <a class="logo header__logo" href="/"></a>
          <div class="header__name">Инамед <span>Клиника урологии</span></div><a class="header__call" href="tel:+74952041494" aria-label="Позвонить"></a>
          <button class="header__switcher js-header-switcher hidden-l" type="button" aria-label="Открыть или закрыть мобильное меню"></button>
          <?php require __DIR__ . '/includes/menu.php'?>
        </div>
      </div>
      <div class="container header__bottom">
        <div class="content">
          <p class="text-marked text-marked--size--l text-align-center">Излечиваем хронический простатит (СХТБ) с <strong>эффективностью 87%</strong>.
          </p>
        </div>
      </div>
    </header>

    <main>
      <div class="container breadcrumbs">
        <div class="content breadcrumbs__inner"><a class="breadcrumbs__parent" href="/">Главная</a>
          <div class="breadcrumbs__current">Вакансия врач-уролог</div>
        </div>
      </div>
      <section class="container page-section">
        <div class="content">
          <h1>Врач-уролог</h1>
          <div class="page-subsection">
            <p class="text-marked text-marked--size--xl">от 80 000 до 250 000 ₽ на руки
            </p>
            <p>Требуемый опыт работы: 1–3 года <br> Частичная занятость, сменный график</p>
            <h2>О&nbsp;нас:</h2>
            <p>Мы&nbsp;&mdash; специализированная урологическая клиника, в&nbsp;которой для лечения пациентов с&nbsp;хроническим небактериальным простатитом (синдромом хронической тазовой боли) применяется <strong>эксклюзивная авторская методика с&nbsp;высокой эффективностью</strong>.</p>
            <p><strong>Обучение данной методике</strong> у&nbsp;ее&nbsp;автора&nbsp;&mdash; является входным этапом при приеме на&nbsp;работу.</p>
            <p>Мы&nbsp;ищем врача-уролога для ведения амбулаторного приема пациентов.</p>
            <p>Мы&nbsp;будем рады специалисту, который открыт к&nbsp;новым знаниям и&nbsp;опыту, чьи <strong>внутренние ценности</strong> совпадает с&nbsp;нашим главным принципом&nbsp;&mdash; &laquo;Нужды пациента превыше всего&raquo;. Способного проявить чуткость и&nbsp;обслуживать непростую группу пациентов.</p>
            <p>Для нас важно соответствие внутренних ценностей кандидата ценностям корпоративной культуры компании. Поэтому мы&nbsp;уделяем особое внимание следующим характеристикам кандидата:</p>
            <ul class="unordered-list par">
              <li>Обладание эмпатией;</li>
              <li>Постоянное усовершенствование процессов и&nbsp;услуг, связанных с&nbsp;лечением пациентов;</li>
              <li>Сохранение и&nbsp;развитие профессиональных навыков и&nbsp;знаний, обучение и&nbsp;научные исследования;</li>
              <li>Работа в&nbsp;команде и&nbsp;стремление к&nbsp;сотрудничеству с&nbsp;коллегами, открытое общение;</li>
              <li>Личная ответственность, честность, доверие;</li>
              <li>Соблюдение норм личного и&nbsp;профессионального поведения, тактичность;</li>
              <li>Заинтересованность в&nbsp;карьерном продвижении;</li>
            </ul>
            <h2>Функциональные задачи:</h2>
            <ul class="unordered-list par">
              <li>Амбулаторный приём пациентов (консультация, осмотр);</li>
              <li>Назначение плана обследования, постановка диагноза, назначение лечения;</li>
              <li>Проведение лечебных манипуляций;</li>
              <li>Ведение электронной документации в&nbsp;МИС;</li>
            </ul>
            <h2>Что мы&nbsp;ждем от&nbsp;будущего коллеги:</h2>
            <ul class="unordered-list par">
              <li>Высшее медицинское образование;</li>
              <li>Действующий сертификат или свидетельство об&nbsp;аккредитации по&nbsp;специальности &laquo;Урология&raquo;;</li>
              <li>Опыт работа от&nbsp;1&nbsp;года;</li>
              <li>Знание современных методов лечения заболеваний, соблюдение стандартов оказания медицинской помощи и&nbsp;клинических рекомендаций;</li>
            </ul>
            <p>* УЗИ диагностика по&nbsp;профилю будет вашим преимуществом.</p>
            <h2>Что мы&nbsp;предлагаем?</h2>
            <ul class="unordered-list par">
              <li>Работа в&nbsp;коллективе единомышленников, которые нашли свое призвание;</li>
              <li>Оформление по&nbsp;ТК&nbsp;РФ&nbsp;и&nbsp;социальные гарантии (трудовая книжка, больничный лист, отпуск);</li>
              <li>Достойная заработная плата без задержек (оклад + %);</li>
              <li>Удобный гибкий график: работа по&nbsp;сменам;</li>
              <li>На&nbsp;первое время&nbsp;&mdash; возможность работы по&nbsp;совместительству. После адаптации и&nbsp;при достижении оптимальной загрузки пациентами&nbsp;&mdash; совмещение будет невозможно.</li>
              <li>Современное оборудование;</li>
              <li>Комфортная атмосфера для спокойной и&nbsp;плодотворной работы;</li>
              <li>Внимательное отношение к&nbsp;пациентам (отсутствие финансовых планов);</li>
              <li>Уважение и&nbsp;поддержка со&nbsp;стороны руководства клиники;</li>
            </ul>
            <h2>Наши преимущества:</h2>
            <ul class="unordered-list par">
              <li>Лояльный и&nbsp;справедливый руководитель <strong>с&nbsp;медицинским образованием</strong>;</li>
              <li>Получение <strong>уникальных знаний и&nbsp;практического опыта</strong> (накопленных в&nbsp;течении более 30&nbsp;лет практики и&nbsp;исследовательской работы) в&nbsp;области урологии, наставничество со&nbsp;стороны автора революционной методики лечения хронического небактериального простатита. Мы&nbsp;готовы вкладывать в&nbsp;обучение и&nbsp;развитие наших врачей;</li>
              <li>Возможность вернуть к&nbsp;нормальной жизни пациентов, которым ранее никто не&nbsp;мог помочь;</li>
              <li>Поощрение ведения научной деятельности;</li>
              <li>Отсутствие бюрократии и&nbsp;возможность влиять на&nbsp;организацию лечебного процесса;</li>
              <li>Личный брендинг врача в&nbsp;период работы, повышение рейтингов, количества отзывов, узнаваемости <strong>(собственное агентство медицинского маркетинга)</strong>.</li>
            </ul>
          </div>
          <div class="page-subsection"><a class="button" href="#vacancy" data-modal>Откликнуться</a>
            <div class="modal" id="vacancy">
              <div class="modal__inner">
                <h2>Заинтересовались вакансией?</h2>
                <p>Оставьте ваши контактные данные и мы свяжемся с вами.</p>
                <form class="grid grid--small-gap--y grid--justify--center grid--align-items--flex-start form par" method="post" action="" data-answer-success="Спасибо за ваш отклик">
                  <label class="form-input form-input--text grid__cell grid__cell--s--6 form__input">
                    <span class="form-input__label"><strong>Ваше имя *</strong>
                    </span>
                    <input class="form-input__field" type="text" name="name" required="required" placeholder="Ваше имя"></input>
                  </label>
                  <label class="form-input form-input--tel grid__cell grid__cell--s--6 form__input">
                    <span class="form-input__label"><strong>Ваш телефон *</strong>
                    </span>
                    <input class="form-input__field" type="tel" name="tel" required="required" placeholder="+7 (9__) ___-__-__"></input>
                  </label>
                  <label class="form-input form-input--file grid__cell grid__cell--s--6 form__input">
                    <span class="form-input__label"><strong>Ваше резюме *</strong>
                    </span>
                    <input class="form-input__field" type="file" name="file" required="required"></input>
                  </label>
                  <button class="grid__cell grid__cell--s--6 grid__cell--xs--8 button form__submit" type="submit">Отправить</button>
                  <label class="form-input form-input--checkbox grid__cell form__agreement">
                    <span class="form-input__label">Отправляя эту форму, вы соглашаетесь с условиями 
                      <a class="link" href="#">Обработки персональных данных.
                      </a>
                    </span>
                    <input class="form-input__field" type="checkbox" required="required" checked="checked"></input>
                  </label>
                </form>
                <button class="modal__close" type="button"></button>
              </div>
            </div>
          </div>
          <div class="block-marked page-subsection">
            <p>Вы можете отправить резюме на e-mail: <a href="mailto:cv@inamed.ru">cv@inamed.ru</a> или позвонить по тел. <a href="tel:+74952041494">+7 (495) 204-14-94</a>.</p>
            <p>Присоединяйтесь к нашей дружной команде профессионалов и специалистов своего дела!</p>
          </div>
        </div>
      </section>
      <section class="container page-section page-section--no-padding--top grid home-contacts">
        <div class="content">
          <div class="grid__cell grid__cell--m--6 grid__cell--xs--12">
            <div class="home-contacts__inner">
              <h2>Наш адрес</h2>
              <p class="text-marked text-marked--size--l">г. Москва, ул. Веерная, д.&nbsp;1, к.&nbsp;2
              </p>
              <div class="home-contacts__icon home-contacts__icon--call"><a class="link" href="tel:+74952041494">+7 (495) 204-14-94</a></div>
              <div class="home-contacts__icon home-contacts__icon--mail"><a class="link" href="mailto:ask@inamed.ru">ask@inamed.ru</a></div>
            </div>
            <div class="home-contacts__map" id="home-contacts-map"></div>
          </div>
        </div>
      </section>
    </main>
    <footer class="container page-section page-section--background-color footer">
      <div class="content footer__inner">
        <div class="footer__row footer__row--name">
          <div class="footer__name">Инамед <span>Клиника урологии</span></div>
          <div class="footer__slogan">Лечим иначе!</div>
        </div>
        <!-- <div class="footer__row footer__row--license footer__item footer__item--license">
          <a href="#">Лицензии и сертификаты</a>
        </div> -->
        <div class="footer__row footer__row--copyright">
          <div class="footer__copyright">
            <p class="footer__year">2024, ООО &laquo;Инамед&raquo;</p><a href="#">Правила использования сайтом</a><a href="#">Соглашение на обработку персональных данных</a>
          </div>
          <div class="footer__info">Имеются противопоказания, необходима консультация специалиста.</div>
        </div>
      </div>
    </footer>
  </body>
  <link rel="stylesheet" href="/local/front/template/styles/style.rest.css?v=<?php require __DIR__ . '/version.php'?>"/>
  <link rel="stylesheet" href="/local/front/template/styles/vacancy.rest.css?v=<?php require __DIR__ . '/version.php'?>"/>
  <script src="/local/front/template/scripts/script.js?v=<?php require __DIR__ . '/version.php'?>"></script>
  <script src="/local/front/template/scripts/vacancy.js?v=<?php require __DIR__ . '/version.php'?>"></script>
</html>
