<!DOCTYPE html>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet"
          href="/local/front/template/styles/style.core.css?v=<?php require __DIR__ . '/version.php' ?>"/>

    <title>Инамед</title>
    <?php require __DIR__ . '/includes/metrika.php' ?>

</head>
<body>
<header class="header">
    <div class="container header__inner">
        <div class="content">
            <a class="logo header__logo" href="/"></a>
            <div class="header__name">
                <a href="/">Инамед </a>
                <span>Клиника урологии</span></div>
            <a class="header__call" href="tel:+74952041494" aria-label="Позвонить"></a>
            <button class="header__switcher js-header-switcher hidden-l" type="button"
                    aria-label="Открыть или закрыть мобильное меню"></button>
            <?php require __DIR__ . '/includes/menu.php' ?>

        </div>
    </div>
    <div class="container header__bottom">
        <div class="content">
            <p class="text-marked text-marked--size--l text-align-center">Излечиваем хронический простатит (СХТБ) с
                <strong>эффективностью 87%</strong>.
            </p>
        </div>
    </div>
</header>
<main>
    <section class="container page-section error-page">
        <div class="content">
            <h1 class="error-page__title">404</h1>
            <p class="error-page__subtitle">К сожалению, страница не найдена.</p>
            <p class="error-page__info">Эта страница удалена, но на нашем сайте еще много интересного.</p><a
                    class="button par" href="/">Вернуться на главную</a>
        </div>
    </section>
</main>
<footer class="container page-section page-section--background-color footer">
    <div class="content footer__inner">
        <div class="footer__row footer__row--name">
            <div class="footer__name">Инамед <span>Клиника урологии</span></div>
            <div class="footer__slogan">Лечим иначе!</div>
        </div>
        <!--.footer__row.footer__row--license.footer__item.footer__item--license
        // a(href='#') Лицензии и сертификаты

        -->
        <div class="footer__row footer__row--copyright">
            <div class="footer__copyright">
                <p class="footer__year">2024, ООО «Инамед»</p><a href="#">Правила
                    использования сайтом</a><a href="#">Соглашение на обработку
                    персональных данных</a>
            </div>
            <div class="footer__info">Имеются противопоказания, необходима консультация специалиста.</div>
            <div class="footer__info" style="display: block">Наша клиника расплагается на&nbsp;базе сети диагностических
                центров <a href="https://gorlab.ru/" target="_blank">«Горлаб»</a></div>
        </div>
    </div>
</footer>

<link rel="stylesheet" href="/local/front/template/styles/style.rest.css?v=<?php require __DIR__ . '/version.php' ?>">
<script src="/local/front/template/scripts/script.js?v=<?php require __DIR__ . '/version.php' ?>"></script>
</body>
</html>
